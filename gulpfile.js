const gulp = require("gulp");
const postcss = require("gulp-postcss");
const autoprefixer = require("autoprefixer");
const prefixer = require("postcss-prefix-selector");
const replace = require("gulp-replace");
const rename = require("gulp-rename");
const styledDir = "build/pre-styles/css/web-formio.css";
const noStyledDir = "build/pre-styles-ns/css/web-formio.css";

const transform = function (
  prefix,
  selector,
  prefixedSelector,
  filePath,
  rule,
) {
  if (
    selector === ".gu-mirror" ||
    selector === ".gu-transit" ||
    selector === ".gu-unselectable" ||
    selector === ".gu-hide" ||
    selector.startsWith(".modal") ||
    selector === ".fade" ||
    selector === "body" ||
    selector.startsWith(".autocomplete")
  ) {
    return selector;
  } else {
    return prefixedSelector;
  }
};

/*** STYLED THEME WITH BTS_ITALIA ***/
gulp.task("default", function () {
  return gulp
    .src(styledDir)
    .pipe(
      postcss([
        autoprefixer,
        prefixer({
          prefix: ".ocl",
          // Optional transform callback for case-by-case overrides
          transform: transform,
        }),
      ]),
    )
    .pipe(gulp.dest("build/bootstrap-italia@2/css"));
});
gulp.task("amaranto", function () {
  return gulp
    .src(styledDir)
    .pipe(
      postcss([
        autoprefixer,
        prefixer({
          prefix: ".ocl",
          // Optional transform callback for case-by-case overrides
          transform: transform,
        }),
      ]),
    )
    .pipe(replace("#06c", "#a61f3a")) // primary
    .pipe(replace("#004d99", "#971c34")) //btn hover
    .pipe(replace("#125ca6", "#971c34")) //btn hover
    .pipe(replace("#036", "#6c1425")) // btn active
    .pipe(replace("#0053a5", "#81182d")) // btn hover
    .pipe(replace("#0066cc80", "#6c1425")) // btn focus
    .pipe(replace("var(--bs-primary)", "#a61f3a")) // var primary
    .pipe(replace("hsl(210, 100%, 40%)", "hsl(348, 69%, 40%)")) // svg
    .pipe(replace("rgba(0, 102, 204, 0.5)", "rgba(166, 31, 58, 0.5)")) // svg
    .pipe(
      rename(function (path) {
        // Updates the object in-place
        path.dirname += "/";
        path.basename += "-amaranto";
        path.extname = ".css";
      }),
    )
    .pipe(gulp.dest("build/bootstrap-italia@2/css"));
});

/*** STYLED THEME WITHOUT BTS_ITALIA ***/
gulp.task("default-ns", function () {
  return gulp
    .src(noStyledDir)
    .pipe(
      postcss([
        autoprefixer,
        prefixer({
          prefix: ".ocl",
          // Optional transform callback for case-by-case overrides
          transform: transform,
        }),
      ]),
    )
    .pipe(gulp.dest("build/css"));
});
gulp.task("amaranto-ns", function () {
  return gulp
    .src(noStyledDir)
    .pipe(
      postcss([
        autoprefixer,
        prefixer({
          prefix: ".ocl",
          // Optional transform callback for case-by-case overrides
          transform: transform,
        }),
      ]),
    )
    .pipe(replace("#06c", "#a61f3a")) // primary
    .pipe(replace("#004d99", "#971c34")) //btn hover
    .pipe(replace("#125ca6", "#971c34")) //btn hover
    .pipe(replace("#036", "#6c1425")) // btn active
    .pipe(replace("#0053a5", "#81182d")) // btn hover
    .pipe(replace("#0066cc80", "#6c1425")) // btn focus
    .pipe(replace("var(--bs-primary)", "#a61f3a")) // var primary
    .pipe(replace("hsl(210, 100%, 40%)", "hsl(348, 69%, 40%)")) // svg
    .pipe(replace("rgba(0, 102, 204, 0.5)", "rgba(166, 31, 58, 0.5)")) // svg
    .pipe(
      rename(function (path) {
        // Updates the object in-place
        path.dirname += "/";
        path.basename += "-amaranto";
        path.extname = ".css";
      }),
    )
    .pipe(gulp.dest("build/css"));
});

gulp.task("themes", gulp.parallel("default", "amaranto"));
gulp.task("themes-ns", gulp.parallel("default-ns", "amaranto-ns"));
