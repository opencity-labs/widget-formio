const webpack = require("webpack");
const path = require("path");

module.exports = {
  webpack: {
    plugins: {
      add: [
        new webpack.optimize.LimitChunkCountPlugin({
          maxChunks: 1,
        }),
      ],
      skipEnvChecks: true,
      skipEnvCheck: true,
    },

    alias: {
      "@": path.resolve(__dirname, "./src"),
    },
    configure: (webpackConfig, { env, paths }) => {
      webpackConfig.module.rules = [
        ...webpackConfig.module.rules,
        {
          test: /\.m?js/,
          resolve: {
            fullySpecified: false,
          },
        },
      ];
      paths.appBuild = webpackConfig.output.path = path.resolve("./dist");
      if (process.env.REACT_APP_ENV === "production") {
        webpackConfig.entry = "./src/index.js";
        webpackConfig.output = {
          filename: "static/js/personal-area.js",
        };
        webpackConfig.plugins[5].options.filename =
          "static/css/personal-area.css";
      } else {
        webpackConfig.entry = "./src/index-green.js";
        webpackConfig.output = {
          filename: "static/js/personal-area.green.js",
        };
        webpackConfig.plugins[5].options.filename =
          "static/css/personal-area.green.css";
      }
      return webpackConfig;
    },
  },
  plugins: [],
};
