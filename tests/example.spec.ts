import { test, expect } from "@playwright/test";

test("test", async ({ page }) => {
  await page.goto("https://servizi.comune-qa.bugliano.pi.it/lang/it/user/");
  await page.getByRole("button", { name: "Quarto Soggetto" }).click();
  await page.goto(
    "https://widget-formio-qa.bugliano.pi.it/?path=/story/example-form-complesso--servizi",
  );
  await page
    .frameLocator('iframe[title="storybook-preview-iframe"]')
    .getByRole("button", { name: "Prosegui senza login" })
    .click();
  await page.getByRole("button", { name: "Form semplice" }).click();
  await expect(
    page
      .frameLocator('iframe[title="storybook-preview-iframe"]')
      .locator("#story--example-form-semplice--servizi--primary-inner")
      .getByRole("list")
      .getByText("Richiedente"),
  ).toBeVisible();
  await expect(
    page
      .frameLocator('iframe[title="storybook-preview-iframe"]')
      .getByText("/2"),
  ).toBeVisible();
  await expect(
    page
      .frameLocator('iframe[title="storybook-preview-iframe"]')
      .getByRole("heading", { name: "Accesso anonimo con unique id" }),
  ).toBeVisible();
  await expect(
    page
      .frameLocator('iframe[title="storybook-preview-iframe"]')
      .getByLabel("unique_id"),
  ).toBeVisible();
  await expect(
    page
      .frameLocator('iframe[title="storybook-preview-iframe"]')
      .getByText("unique_id"),
  ).toBeVisible();
  await expect(
    page
      .frameLocator('iframe[title="storybook-preview-iframe"]')
      .getByText("Genere"),
  ).toBeVisible();
  await expect(
    page
      .frameLocator('iframe[title="storybook-preview-iframe"]')
      .locator("label")
      .filter({ hasText: "Maschio" }),
  ).toBeVisible();
  await expect(
    page
      .frameLocator('iframe[title="storybook-preview-iframe"]')
      .locator("label")
      .filter({ hasText: "Femmina" }),
  ).toBeVisible();
  await expect(
    page
      .frameLocator('iframe[title="storybook-preview-iframe"]')
      .locator("i")
      .nth(1),
  ).toBeVisible();
  await expect(
    page
      .frameLocator('iframe[title="storybook-preview-iframe"]')
      .locator("#l-eryeg64-labelDiTest1 i"),
  ).toBeVisible();
  await expect(
    page
      .frameLocator('iframe[title="storybook-preview-iframe"]')
      .locator("#d-e9mffq-labelDiTest"),
  ).toBeVisible();
  await expect(
    page
      .frameLocator('iframe[title="storybook-preview-iframe"]')
      .locator("#l-eryeg64-labelDiTest1"),
  ).toBeVisible();
  await expect(
    page
      .frameLocator('iframe[title="storybook-preview-iframe"]')
      .getByRole("button", { name: "Nexticon" }),
  ).toBeVisible();
  await expect(
    page
      .frameLocator('iframe[title="storybook-preview-iframe"]')
      .getByText("iconBackNexticon"),
  ).toBeVisible();
  await expect(
    page
      .frameLocator('iframe[title="storybook-preview-iframe"]')
      .locator("#l-ef7se98-surname"),
  ).toContainText("Cognome");
  await expect(
    page
      .frameLocator('iframe[title="storybook-preview-iframe"]')
      .locator("#l-etrlywo-name"),
  ).toContainText("Nome");
  await expect(
    page
      .frameLocator('iframe[title="storybook-preview-iframe"]')
      .getByPlaceholder("Mario", { exact: true }),
  ).toHaveValue("Marco");
  await expect(
    page
      .frameLocator('iframe[title="storybook-preview-iframe"]')
      .getByPlaceholder("Rossi", { exact: true }),
  ).toHaveValue("Polo");
  await expect(
    page
      .frameLocator('iframe[title="storybook-preview-iframe"]')
      .getByPlaceholder("mario.rossi@example.mail"),
  ).toHaveValue("test@test.it");
  await expect(
    page
      .frameLocator('iframe[title="storybook-preview-iframe"]')
      .getByPlaceholder("__/__/____"),
  ).toHaveValue("01/09/1976");
  await expect(
    page
      .frameLocator('iframe[title="storybook-preview-iframe"]')
      .getByPlaceholder("MRARSI01A70G452D"),
  ).toHaveValue("PLOMRC01P30L736Y");
  await expect(
    page
      .frameLocator('iframe[title="storybook-preview-iframe"]')
      .getByPlaceholder("Roma", { exact: true }),
  ).toHaveValue("Ponsacco");
  await expect(
    page
      .frameLocator('iframe[title="storybook-preview-iframe"]')
      .getByPlaceholder("Campitello di Fassa"),
  ).toHaveValue("Bugliano");
  await expect(
    page
      .frameLocator('iframe[title="storybook-preview-iframe"]')
      .getByPlaceholder("Via Roma"),
  ).toHaveValue("Via Gramsci, 1");
  await expect(
    page
      .frameLocator('iframe[title="storybook-preview-iframe"]')
      .getByPlaceholder("__", { exact: true }),
  ).toHaveValue("TN");
  await expect(
    page
      .frameLocator('iframe[title="storybook-preview-iframe"]')
      .getByPlaceholder("38031"),
  ).toHaveValue("56056");
});
