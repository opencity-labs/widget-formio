const rewireWebpackBundleAnalyzer = require("react-app-rewire-webpack-bundle-analyzer");
const multipleEntry = require("react-app-rewire-multiple-entry")([
  {
    entry: "src/index.js",
    template: "public/index.html",
    outPath: "/landing.html",
    omitHash: true,
  },
]);

module.exports = function override(config, env) {
  multipleEntry.addMultiEntry(config);

  config = rewireWebpackBundleAnalyzer(config, env, {
    analyzerMode: "static",
    reportFilename: "report.html",
  });

  config.optimization.runtimeChunk = false;
  config.optimization.splitChunks = {
    cacheGroups: {
      default: false,
    },
  };
  config.output = {
    ...config.output, // copy all settings
    filename: "static/js/web-formio.js",
  };

  return config;
};
