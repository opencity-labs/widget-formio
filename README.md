# Widget Form.io Core basato su Bootstrap Italia

Questo repository implementa un componente web basato su [formio/react](https://github.com/formio/react).

## Script Disponibili

Nella directory del progetto, puoi eseguire:

### `npm start`

Esegue l'app in modalità di sviluppo.  
Apri [http://localhost:3000](http://localhost:3000) per visualizzarla nel browser.

La pagina si ricaricherà automaticamente ogni volta che apporti modifiche.  
Potresti anche vedere eventuali errori di lint nella console.

## Utilizzo

Per utilizzare Design React come dipendenza nel tuo progetto React, puoi installarlo da [npm](https://www.npmjs.com/~italia).

Maggiori informazioni su come creare una nuova app con React:

- [Documentazione ufficiale](https://react.dev/learn/start-a-new-react-project)

### Widget Servizio

```html
<script
  defer
  src="https://static.opencityitalia.it/widgets/formio/latest/js/web-formio.js"
></script>
<link
  rel="stylesheet"
  href="https://static.opencityitalia.it/widgets/formio/latest/css/web-formio.css"
/>

<widget-formio
  service-id="0121edc9-57d8-45f2-ae92-260f45aec6d5"
  base-url="https://servizi.comune-qa.bugliano.pi.it/lang"
  formserver-url="https://form-qa.stanzadelcittadino.it"
  pdnd-url="https://api.qa.stanzadelcittadino.it/pdnd/v1"
>
</widget-formio>
```

Documentazione:

- [Storybook](https://widget-formio.comune-qa.bugliano.pi.it/)

### Analizzare la Dimensione del Bundle

Questa sezione è stata spostata qui: [https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size](https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size)
