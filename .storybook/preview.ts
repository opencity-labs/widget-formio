import type { Preview } from "@storybook/react";
import i18n from "../src/i18n/i18nService/i18n";

const preview: Preview = {
  parameters: {
    i18n,
    controls: {
      matchers: {
        color: /(background|color)$/i,
        date: /Date$/i,
      },
    },
  },
  tags: ["autodocs", "autodocs"],
  initialGlobals: {
    locale: "it",
    locales: {
      it: { icon: "🇮🇹", title: "Italiano", right: "IT" },
      en: { icon: "🇬🇧", title: "English", right: "EN" },
      de: { icon: "🇩🇪", title: "Tedesco", right: "DE" },
    },
  },
};

export default preview;
