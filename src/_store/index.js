import { configureStore } from "@reduxjs/toolkit";
import { authReducer } from "./auth.slice";
import { apiReduced } from "./api.slice";
import { currentUserReducer } from "./currentuser.slice";
import { formReducer } from "./form.slice";
import { formserverReducer } from "./formserver.slice";

export * from "./auth.slice";
export * from "./api.slice";
export * from "./currentuser.slice";
export * from "./form.slice";
export * from "./formserver.slice";

export const store = configureStore({
  reducer: {
    auth: authReducer,
    currentUser: currentUserReducer,
    form: formReducer,
    api: apiReduced,
    formserver: formserverReducer,
  },
  // here we restore the previously persisted state
  // preloadedState: loadState(),
});
