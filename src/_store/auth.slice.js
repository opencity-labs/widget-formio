import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { jwtDecode } from "jwt-decode";
import { fetchWrapper } from "../_helpers/fetch-wrapper";
import { baseUrl } from "./api.slice";

// create slice
const name = "auth";
const initialState = createInitialState();
const extraActions = createExtraActions();
const slice = createSlice({
  name,
  initialState,
  reducers: {
    logout: (state, action) => {
      //state.token = null;
      state.user_data = null;
      state.loading = false;
      state.show_login = false;
    },
  },
  extraReducers: (builder) => {
    builder
      /** getSessionToken **/
      .addCase(`${name}/getSessionToken/pending`, (state, action) => {
        state.loading = true;
        state.show_login = false;
      })
      .addCase(`${name}/getSessionToken/fulfilled`, (state, action) => {
        const data = action.payload;
        state.user_data = jwtDecode(data.token);
        state.token = data.token;
        state.loading = false;
        state.show_login = false;
      })
      .addCase(`${name}/getSessionToken/rejected`, (state, action) => {
        state.loading = false;
        if (process.env.NODE_ENV === "development") {
          if (process.env.REACT_APP_DEV_TOKEN) {
            state.user_data = jwtDecode(`${process.env.REACT_APP_DEV_TOKEN}`);
            state.token = `${process.env.REACT_APP_DEV_TOKEN}`;
          }
          console.log(process.env.NODE_ENV);
        }
        state.show_login = true;
      })
      /** createSessionToken **/
      .addCase(`${name}/createSessionToken/pending`, (state, action) => {
        state.loading = true;
      })
      .addCase(`${name}/createSessionToken/fulfilled`, (state, action) => {
        const data = action.payload;
        state.user_data = jwtDecode(data.token);
        state.token = data.token;
        state.loading = false;
      })
      .addCase(`${name}/createSessionToken/rejected`, (state, action) => {
        state.loading = false;
        if (process.env.NODE_ENV === "development") {
          state.user_data = jwtDecode(`${process.env.REACT_APP_DEV_TOKEN}`);
          state.token = `${process.env.REACT_APP_DEV_TOKEN}`;
          console.log(process.env.NODE_ENV);
        }
      });
  },
});

// exports
export const authActions = { ...slice.actions, ...extraActions };
export const authReducer = slice.reducer;

// implementation

function createInitialState() {
  return {
    // initialize state from local storage to enable user to stay logged in
    token: "",
    user_data: null,
    loading: false,
    show_login: false,
  };
}

function createExtraActions() {
  return {
    getSessionToken: getSessionToken(),
    createSessionToken: createSessionToken(),
  };

  function getSessionToken() {
    return createAsyncThunk(
      `${name}/getSessionToken`,
      async () =>
        await fetchWrapper.getWithCredentials(
          `${baseUrl()}/api/session-auth?with-cookie=true`,
        ),
    );
  }

  function createSessionToken() {
    return createAsyncThunk(
      `${name}/createSessionToken`,
      async () => await fetchWrapper.post(`${baseUrl()}/api/session-auth`, {}),
    );
  }
}

export const { logout } = slice.actions;
