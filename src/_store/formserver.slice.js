import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";

import { fetchWrapper } from "../_helpers/fetch-wrapper";
import { getI18n } from "react-i18next";

// create slice
const name = "formserver";
const initialState = createInitialState();
const extraActions = createExtraActions();

const slice = createSlice({
  name,
  initialState,
  extraReducers: (builder) => {
    builder
      /** getFormById **/
      .addCase(`${name}/getFormById/fulfilled`, (state, action) => {
        state.form = action.payload;
      })
      .addCase(`${name}/getFormById/rejected`, (state, action) => {
        state.form = { error: action.error };
      })
      /** getTranslationsFormById **/
      .addCase(`${name}/getTranslationsFormById/fulfilled`, (state, action) => {
        state.translations = action.payload;
      })
      .addCase(`${name}/getTranslationsFormById/rejected`, (state, action) => {
        state.translations = { error: action.error };
      });
  },
});

// exports
export const formserverActions = { ...slice.actions, ...extraActions };
export const formserverReducer = slice.reducer;

// implementation

function createInitialState() {
  return {
    form: null,
    translations: null,
  };
}

function createExtraActions() {
  return {
    getFormById: getFormById(),
    getTranslationsFormById: getTranslationsFormById(),
  };

  function getFormById() {
    return createAsyncThunk(
      `${name}/getFormById`,
      async (id) => await fetchWrapper.get(`${formserverUrl()}/form/${id}`),
    );
  }

  function getTranslationsFormById() {
    return createAsyncThunk(
      `${name}/getTranslationsFormById`,
      async (id) =>
        await fetchWrapper.get(
          `${formserverUrl()}/form/${id}/i18n?lang=${getI18n().language}`,
        ),
    );
  }
}

export const formserverUrl = () => {
  return window.FORMSERVER_URL || `${process.env.REACT_APP_FORMSERVER_URL}`;
};
