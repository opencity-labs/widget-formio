import { createSlice, current } from "@reduxjs/toolkit";

const initialState = () => {
  return {
    name: "",
    surname: "",
    email_address: "",
    phone_number: "",
    fiscal_code: "",
    privacy: false,
    addressForm: "",
    type: "",
    subject: "",
    details: "",
    images: [],
    docs: [],
    application: {},
    sequential_id: null,
  };
};
const rootSlice = createSlice({
  name: "form",
  initialState,
  reducers: {
    storePrivacy: (state, action) => {
      state.privacy = action.payload;
    },
    storeAddress: (state, action) => {
      state.addressForm = action.payload;
    },
    storeType: (state, action) => {
      state.type = action.payload;
    },
    storeDetails: (state, action) => {
      state.details = action.payload;
    },
    storeSeverity: (state, action) => {
      state.severity = action.payload;
    },
    storeName: (state, action) => {
      state.name = action.payload;
    },
    storeSurname: (state, action) => {
      state.surname = action.payload;
    },
    storeEmail: (state, action) => {
      state.email_address = action.payload;
    },
    storePhoneNumber: (state, action) => {
      state.phone_number = action.payload;
    },
    storeFiscalCode: (state, action) => {
      state.fiscal_code = action.payload;
    },
    storeSubject: (state, action) => {
      state.subject = action.payload;
    },
    storeImages: (state, action) => {
      const currentState = current(state);
      const images = [...currentState.images, action.payload];
      state.images = images;
    },
    storeDocs: (state, action) => {
      const currentState = current(state);
      const docs = [...currentState.docs, action.payload];
      state.docs = docs;
    },
    removeImage: (state, action) => {
      const currentState = current(state);
      state.images = currentState.images.filter((el) => el !== action.payload);
    },
    removeDocs: (state, action) => {
      const currentState = current(state);
      state.docs = currentState.docs.filter((el) => el !== action.payload);
    },
    storeApplication: (state, action) => {
      state.application = action.payload;
    },
    reset: (state) => initialState(),
    storeSequentialId: (state, action) => {
      state.sequential_id = action.payload;
    },
    storeLoadImages: (state, action) => {
      let customData = action.payload.map((option) => ({
        ...option,
        data: {
          id: option.id,
          uri: option.url,
        },
      }));

      state.images = customData;
    },
    storeLoadDocs: (state, action) => {
      let customData = action.payload.map((option) => ({
        ...option,
        data: {
          id: option.id,
          uri: option.url,
        },
      }));

      state.docs = customData;
    },
  },
});

export const formReducer = rootSlice.reducer;

export const {
  storePrivacy,
  storeAddress,
  storeType,
  storeDetails,
  storeSeverity,
  storeName,
  storeSurname,
  storePhoneNumber,
  storeFiscalCode,
  storeEmail,
  storeSubject,
  storeImages,
  storeDocs,
  removeImage,
  removeDocs,
  storeApplication,
  storeLoadDocs,
  storeLoadImages,
  storeSequentialId,
  reset,
} = rootSlice.actions;
