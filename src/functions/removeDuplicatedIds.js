export function removeDuplicatedIds() {

    for (const [key, value] of Object.entries(getDuplicateIds())) {
        if(value >= 2){
            let box = document.getElementById(key);
            if (box) {
                box.id = generateRandomString(); // Change the ID of the div
            }
        }
       // console.log(`${key}: ${value}`);
    }
    getDuplicateIds()

}


function generateRandomString() {
    const chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    let result = '';
    for (let i = 0; i < 7; i++) {
        result += chars.charAt(Math.floor(Math.random() * chars.length));
    }
    return result;
}

function getDuplicateIds() {
    let nodes = document.querySelectorAll('.formio-component [id]');

    let ids = {};
    let totalNodes = nodes.length;

    for(let i=0; i<totalNodes; i++) {
        let currentId = nodes[i].id ? nodes[i].id : "undefined";
        if(isNaN(ids[currentId])) {
            ids[currentId] = 0;
        }
        ids[currentId]++;
    }

    return ids
}
