export function formCheckLabelRequired() {
    const elems = document.getElementsByClassName('field-required form-check-label')
    for (let key of elems) {
        if(!key.innerHTML.includes('<span class="field-required"></span>')){
            key.innerHTML += '<span class="field-required"></span>'
        }

    }
}
