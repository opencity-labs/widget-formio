import {Utils} from "@formio/react";

export function disableApplicantField(form) {
    const componentApplicant = Utils.searchComponents(form.components, {
        key: "applicant",
    });
    if(componentApplicant.length){
        if(componentApplicant[0]?.element){
            const queryElementInput =
                componentApplicant[0].element.querySelectorAll(`[name^="data["]`);

            const filteredInput = Array.from(queryElementInput).filter(
                (el) => !el.closest(".formio-component-address"),
            );

            filteredInput.forEach((element) => {
                if (element.type === "radio") {
                    let name = element.name;
                    let radioElements = document.querySelectorAll(
                        "input[name='" + name + "']",
                    );
                    radioElements.forEach((radio) => {
                        radio.setAttribute("disabled", "disabled");
                    });
                } else if (
                    element.value &&
                    element.type !== "email" &&
                    element.name !== "data[cell_number]" &&
                    element.name !== "data[phone_number]"
                ) {
                    element.setAttribute("disabled", true);
                }
            });
        }

    }

}