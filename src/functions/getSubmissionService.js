import { useDispatch } from "react-redux";
import { apiActions } from "../_store";

export const getSubmissionService = async (serviceId) => {
  const dispatch = useDispatch();

  /*   const applicantComponents = Utils.searchComponents(formInstance.current.components, {
               'key': 'applicant'
           });*/
  return dispatch(
    apiActions.getSubmissionByServiceId({ serviceId: serviceId }),
  );
};
