import editForm from "./SdcFile.form";
import { ReactComponent } from "@formio/react";
import { createRoot } from "react-dom/client";
import { baseUrl } from "../../_helpers/BaseUrl";
import { Button, Icon, Progress } from "design-react-kit";
import React, { useEffect, useState } from "react";
import { useDropzone } from "react-dropzone";
import axios from "axios";
import Swal from "sweetalert2";
import mime from "mime";
import { sessionGet } from "../../_helpers/utilities";
import Auth from "../../class/Auth";

let signatureCheckWsUrl = null;
if (document.querySelector("widget-formio") !== null) {
  signatureCheckWsUrl = document
    .querySelector("widget-formio")
    .getAttribute("signature-check-ws-url");
}

export const ReactSdcFile = (props) => {
  const { value, component, current, onChange, i18next } = props;

  const [uploadedFiles, setUploadedFiles] = useState([]);
  const [filesUploadingMap, setFilesUploadingMap] = useState(new Map());
  // Dato componente sola lettura
  const [renderDataReadOnly, setRenderDataReadOnly] = useState(false);
  const auth = new Auth();

  useEffect(() => {
    auth.getSessionAuthTokenPromise();
  }, []);

  const addFilesUploading = (id, file) => {
    setFilesUploadingMap((prevMap) => {
      const updatedMap = new Map(prevMap);
      updatedMap.set(id, file);
      return updatedMap;
    });
  };

  function maxSizeValidator(file) {
    if (
      file.size >
      (translateScalars(component.fileMaxSize) || translateScalars("1GB"))
    ) {
      return {
        code: "size-files",
        message: `${i18next.t("I file non possono superare le dimensioni di")} ${component.fileMaxSize ? component.fileMaxSize : "1GB"}`,
      };
    }
    if (
      uploadedFiles.length >=
      (component.maxFiles
        ? parseInt(component.maxFiles)
        : component.multiple
          ? 100
          : 1)
    ) {
      return {
        code: "too-many-files",
        message: `${i18next.t("Puoi caricare massimo ")} ${component.maxFiles || 100} ${i18next.t("file")}`,
      };
    }
    if (file.size < translateScalars(component.fileMinSize)) {
      return {
        code: "file-too-small",
        message: `${i18next.t("I file devono avare una dimensione superiore a ")} ${component.fileMinSize}`,
      };
    }
    return null;
  }

  function validateTypeFile() {
    if (component.filePattern) {
      const filePattern = component.filePattern.split(",");
      let accepted = {};
      filePattern.map((file) => {
        const type = mime.getType(file);
        accepted[type] = [];
        accepted[type].push(file);
      });
      return accepted;
    } else {
      return true;
    }
  }

  const parsedFileInfo = (fileUpload) => {
    let fileInfo = {};

    fileInfo.originalName = fileUpload.originalName
      ? fileUpload.originalName
      : fileUpload.name;
    fileInfo.name = fileUpload.name;
    fileInfo.url = fileUpload.url;
    fileInfo.data = fileUpload.data;
    fileInfo.signature_validation = fileUpload.signature_validation;
    fileInfo.mime_type = fileUpload.mime_type;
    fileInfo.storage = "url";
    fileInfo.size = fileUpload.size;

    return fileInfo;
  };

  const { getRootProps, getInputProps, fileRejections, isDragActive, open } =
    useDropzone({
      onDrop: (acceptedFiles) => {
        acceptedFiles.map((file) => {
          uploadFile(file).then((res) => {
            // setUploadedFiles([...uploadedFiles, ...acceptedFiles]);
          });
        });
      },
      maxFiles: component.maxFiles
        ? parseInt(component.maxFiles) - uploadedFiles.length
        : component.multiple
          ? 100
          : 1,
      validator: maxSizeValidator,
      accept: validateTypeFile(),
    });

  const removeFile = (file) => {
    const url = baseUrl() + "/api/attachments/" + file.data.id;
    fetch(url, {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json; charset=utf-8",
        Authorization: `Bearer ${sessionGet("auth-token")}`,
      },
      strictErrors: true,
    })
      .then(() => {
        setUploadedFiles((prevUploadedFiles) => {
          let files = prevUploadedFiles.filter(
            (item) => item.data.id !== file.data.id,
          );
          onChange(files);
          return files;
        });
      })
      .catch((error) => {
        Swal.fire({
          title: "",
          html: `<p>${i18next.t("Ci sono stati degli errori durante l'eliminazione del file")}</p>`,
          icon: "error",
          cancelButtonText: i18next.t("Chiudi"),
          showCancelButton: true,
          showConfirmButton: false,
        });
        console.error(error);
      });
  };

  const loadFiles = () => {
    const data =
      value && value.length > 0
        ? value
        : current.dataValue
          ? current.dataValue
          : null;
    if (data?.length > 0) {
      setUploadedFiles(data);
      setTimeout(() => {
        onChange(data);
      }, 100);
    }
  };

  useEffect(() => {
    if (
      document.querySelector(".formio-read-only") &&
      renderDataReadOnly === false
    ) {
      setRenderDataReadOnly(true);
      setTimeout(() => {
        loadFiles();
      }, 1000);
    } else {
      setRenderDataReadOnly(false);
      setTimeout(() => {
        loadFiles();
      }, 1000);
    }
  }, []);

  async function uploadFile(fileUpload) {
    let idUpload;
    const url = baseUrl() + "/it/upload";

    let file = {
      mime_type: fileUpload.type,
      name: fileUpload.name,
      original_filename: fileUpload.name,
      protocol_required: component.protocol_required,
      size: fileUpload.size,
    };

    return await axios
      .post(url, file, {
        maxContentLength: Infinity,
        maxBodyLength: Infinity,
      })
      .then(async (respPost) => {
        idUpload = respPost.data.id;
        fileUpload.url = baseUrl() + "/it/allegati/" + idUpload;
        fileUpload.storage = "url";
        fileUpload.mime_type = fileUpload.type;
        fileUpload.protocol_required = component.protocol_required;
        fileUpload.data = {
          ...respPost.data,
          baseUrl: baseUrl() + "/it/allegati/" + idUpload,
        };
        addFilesUploading(idUpload, fileUpload);
        return await axios
          .put(respPost.data.uri, fileUpload, {
            onUploadProgress: (progressEvent) => {
              let percentCompleted = Math.round(
                (progressEvent.loaded * 100) / progressEvent.total,
              );
              setFilesUploadingMap((prevMap) => {
                const updatedMap = new Map(prevMap);
                const file = fileUpload;
                file.progress = parseInt(percentCompleted);
                if (updatedMap.has(idUpload)) {
                  updatedMap.set(idUpload, file);
                }
                return updatedMap;
              });
            },
            maxContentLength: Infinity,
            maxBodyLength: Infinity,
            headers: { "Content-Type": "multipart/form-data" },
          })
          .then(async (response) => {
            const regex = /\\|"/gi;
            const etag = response.headers.etag.replace(regex, "") || null;
            //  let fileIndex = component.dataValue.push(fileUpload) - 1;
            return await axios
              .put(baseUrl() + "/it/upload/" + idUpload, {
                file_hash: etag,
                check_signature: component.check_signature,
              })
              .then(async (resp) => {
                setFilesUploadingMap((prevMap) => {
                  const updatedMap = new Map(prevMap);
                  updatedMap.delete(idUpload);
                  return updatedMap;
                });
                let currentValue = value || current.dataValue || [];
                let fileIndex = null;
                setUploadedFiles((files) => {
                  fileIndex = files.push(parsedFileInfo(fileUpload)) - 1;
                  onChange(files);
                  return files;
                });
                if (
                  component.check_signature &&
                  resp.data.url &&
                  signatureCheckWsUrl
                ) {
                  return await axios
                    .post(
                      signatureCheckWsUrl,
                      {
                        url: resp.data.url,
                        content: null,
                      },
                      {
                        headers: {
                          "Content-Type": "application/json",
                        },
                      },
                    )
                    .then((resp) => {
                      fileUpload.signature_validation = resp.data.sign;
                      currentValue[fileIndex].signature_validation =
                        resp.data.sign;
                    })
                    .catch((e) => {
                      currentValue[fileIndex].signature_validation =
                        "File validation error: " + e;
                    });
                }

                setFilesUploadingMap((prevMap) => {
                  const updatedMap = new Map(prevMap);
                  updatedMap.delete(idUpload);
                  return updatedMap;
                });
              })
              .catch((err) => {
                setFilesUploadingMap((prevMap) => {
                  const updatedMap = new Map(prevMap);
                  updatedMap.delete(idUpload);
                  return updatedMap;
                });
                console.log(err);
                Swal.fire({
                  title: "",
                  html: `<p>${i18next.t("Ci sono stati degli errori dutante il caricamento dei file, riprova più tardi!")}</p>`,
                  icon: "error",
                  cancelButtonText: i18next.t("Chiudi"),
                  showCancelButton: true,
                  showConfirmButton: false,
                });
              });
          })
          .catch((err) => {
            setFilesUploadingMap((prevMap) => {
              const updatedMap = new Map(prevMap);
              updatedMap.delete(idUpload);
              return updatedMap;
            });
            console.log(err);
            Swal.fire({
              title: "",
              html: `<p>${i18next.t("Ci sono stati degli errori dutante il caricamento dei file, riprova più tardi!")}</p>`,
              icon: "error",
              cancelButtonText: i18next.t("Chiudi"),
              showCancelButton: true,
              showConfirmButton: false,
            });
          });
      })
      .catch((responseErr) => {
        console.log(responseErr);
        Swal.fire({
          title: "",
          html: `<p>${i18next.t("Ci sono stati degli errori dutante il caricamento dei file, riprova più tardi!")}</p>`,
          icon: "error",
          cancelButtonText: i18next.t("Chiudi"),
          showCancelButton: true,
          showConfirmButton: false,
        });
        setFilesUploadingMap((prevMap) => {
          const updatedMap = new Map(prevMap);
          updatedMap.delete(idUpload);
          return updatedMap;
        });
      });
  }

  // trasforma stringa in bytes
  function translateScalars(str) {
    if (typeof str === "string") {
      if (str.search(/kb/i) === str.length - 2) {
        return parseFloat(str.substring(0, str.length - 2) * 1024);
      }
      if (str.search(/mb/i) === str.length - 2) {
        return parseFloat(str.substring(0, str.length - 2) * 1024 * 1024);
      }
      if (str.search(/gb/i) === str.length - 2) {
        return parseFloat(
          str.substring(0, str.length - 2) * 1024 * 1024 * 1024,
        );
      }
      if (str.search(/b/i) === str.length - 1) {
        return parseFloat(str.substring(0, str.length - 1));
      }
      if (str.search(/s/i) === str.length - 1) {
        return parseFloat(str.substring(0, str.length - 1));
      }
      if (str.search(/m/i) === str.length - 1) {
        return parseFloat(str.substring(0, str.length - 1) * 60);
      }
      if (str.search(/h/i) === str.length - 1) {
        return parseFloat(str.substring(0, str.length - 1) * 3600);
      }
    }
    return str;
  }

  function formatBytes(bytes, decimals = 2) {
    if (!+bytes) return "0 Bytes";

    const k = 1024;
    const dm = decimals < 0 ? 0 : decimals;
    const sizes = ["Bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"];

    const i = Math.floor(Math.log(bytes) / Math.log(k));

    return `${parseFloat((bytes / Math.pow(k, i)).toFixed(dm))} ${sizes[i]}`;
  }

  return (
    <>
      {renderDataReadOnly ? (
        <div>
          <ul className="upload-file-list">
            {uploadedFiles.map((file, index) => (
              <li
                key={file.size + index}
                className="upload-file success mw-100"
              >
                <Icon
                  aria-hidden="true"
                  color="primary"
                  icon="it-file"
                  title="Icona file"
                  size="sm"
                />
                <p>
                  <span className="visually-hidden">
                    {i18next.t("File caricato:")}
                  </span>
                  {file.name}
                  <span className={"upload-file-weight"}>
                    {formatBytes(file.size)}
                  </span>
                </p>
              </li>
            ))}
            {Boolean(uploadedFiles.length < 1) && (
              <li className="upload-file text-secondary mw-100">
                <Icon
                  aria-hidden="true"
                  color="light"
                  icon="it-files"
                  title="Icona file"
                  size="sm"
                />
                <p>{i18next.t("Nessun elemento")}</p>
              </li>
            )}
          </ul>
        </div>
      ) : (
        <>
          <div
            className={`my-1 border-2 border-light file-uploader-box rounded p-3 text-secondary${isDragActive ? " dragover" : ""}`}
            style={{ border: "dashed" }}
            {...getRootProps({
              onClick: (event) => event.stopPropagation(),
            })}
          >
            <input
              id={`${component.id}-${component.key}`}
              aria-describedby={`file-uploader-description-${component.id}-${component.key}`}
              {...getInputProps()}
            />
            <div
              className={`upload-dragdrop${isDragActive ? " dragover" : ""}`}
            >
              <div className="ms-1 me-3">
                <Icon
                  color="secondary"
                  icon="it-upload"
                  title={i18next.t("Carica file")}
                  size={"_sm"}
                />
              </div>
              <div className="upload-dragdrop-text">
                <div className="h5" id={`file-uploader-description-${component.id}-${component.key}`}>
                  {i18next.t("Trascina il file per caricarlo")}
                </div>
                <p className="d-inline-flex">
                  {i18next.t("oppure")}
                  <Button
                    tag="input"
                    type="button"
                    value={i18next.t("selezionalo dal dispositivo")}
                    onClick={open}
                    className="px-1 py-0 text-decoration-underline primary-color"
                    size="xs"
                  />
                </p>
              </div>
            </div>
          </div>
          <ul className="upload-file-list my-0">
            {uploadedFiles.map((file, index) => (
              <li key={file.size + index} className="upload-file success">
                <Icon
                  aria-hidden="true"
                  color="primary"
                  icon="it-file"
                  title="Icona file"
                  size="sm"
                />
                <p>
                  <span className="visually-hidden">
                    {i18next.t("File caricato:")}
                  </span>
                  {file.name}
                  <span className={"upload-file-weight"}>
                    {formatBytes(file.size)}
                  </span>
                </p>
                <Button className="px-1" onClick={() => removeFile(file)}>
                  <Icon
                    title={`${i18next.t("Elimina file caricato")} ${file.name}`}
                    color="secondary"
                    icon="it-delete"
                    className="ms-auto pointer"
                    size="sm"
                  />
                </Button>
              </li>
            ))}
            {Array.from(filesUploadingMap.entries()).map(([id, file]) => (
              <li key={id} className="upload-file success">
                <Icon
                  aria-hidden="true"
                  color="primary"
                  icon="it-file"
                  title="Icona file"
                  size="sm"
                />
                <p>
                  <span className="visually-hidden">
                    {i18next.t("Caricamento file:")}
                  </span>
                  {file.name}
                  <span className={"upload-file-weight"}>
                    {formatBytes(file.size)}
                  </span>
                </p>
                <Progress value={file.progress} />
              </li>
            ))}
          </ul>
          {Boolean(fileRejections && fileRejections.length) && 
          <div className="formio-error-wrapper">
          <ul className="formio-errors upload-file-list my-0 ">
            {fileRejections.map(({ file, errors }, index) => (
              <li key={file.size + index} className="upload-file error">
                <div className="upload-file-name-error-wrapper">
                  <Icon
                    aria-hidden="true"
                    color="error"
                    icon="it-error"
                    title="Icona file"
                    size="sm"
                  />
                  <p className="text-wrap error">
                    <span className="visually-hidden">
                      {i18next.t("Errore caricamento file:")}
                    </span>
                    {file.name}
                    <span className={"upload-file-weight"}>
                      {formatBytes(file.size)}
                    </span>
                  </p>
                  {/* <Button aria-hidden="true" disabled className="px-0">
                    <Icon
                      aria-hidden="true"
                      color="error"
                      icon="it-close"
                      className="px-0"
                    />
                  </Button> */}
                </div>
                <div>
                  {errors.map((e) => {
                    if (e.code === "file-invalid-type") {
                      return (
                        <p className="text-wrap ms-0">
                          {i18next.t("I file possono essere di tipo ")}
                          {component.filePattern}
                        </p>
                      );
                    } else if (e.code === "too-many-files") {
                      return (
                        <p className="text-wrap ms-0">
                          {i18next.t("Puoi caricare massimo ")}{" "}
                          {component.maxFiles
                            ? component.maxFiles
                            : component.multiple
                              ? 100
                              : 1}{" "}
                          {i18next.t("file")}
                        </p>
                      );
                    } else {
                      return (
                        <p className="text-wrap ms-0">{e.message}</p>
                      );
                    }
                  })}
                </div>
              </li>
            ))}
          </ul>
          </div>
          }
        </>
      )}
    </>
  );
};

export default class FormioSdcFile extends ReactComponent {
  // eslint-disable-next-line no-useless-constructor
  constructor(component, options, data) {
    super({...component, multiple: true}, options, data);
  }

  /**
   * This function tells the form builder about your component. It's name, icon and what group it should be in.
   *
   * @returns {{title: string, icon: string, group: string, documentation: string, weight: number, schema: *}}
   */
  static get builderInfo() {
    return {
      title: "File Sdc",
      group: "basic",
      icon: "bi bi-file-earmark-text",
      documentation: "",
      weight: 70,
      schema: FormioSdcFile.schema(),
    };
  }

  /**
   * This function is the default settings for the component. At a minimum you want to set the type to the registered
   * type of your component (i.e. when you call Components.setComponent('type', MyComponent) these types should match.
   *
   * @param sources
   * @returns {*}
   */
  static schema() {
    return ReactComponent.schema({
      type: "sdcfile",
    });
  }

  /*
   * Defines the settingsForm when editing a component in the builder.
   */
  static editForm = editForm;

  /**
   * This function is called when the DIV has been rendered and added to the DOM. You can now instantiate the react component.
   *
   * @param DOMElement
   * #returns ReactInstance
   */
  attachReact(element, ref) {
    this.rootComponent = createRoot(element);
    this.rootComponent.render(
      <ReactSdcFile
        ref={ref}
        component={this.component} // These are the component settings if you want to use them to render the component.
        value={this.dataValue || this.dataForSetting} // The starting value of the component.
        onChange={(e) => this.setValue(e)} // The onChange event to call when the value changes.
        {...this}
        current={this}
      />,
    );
  }

  setValue(value) {
    if (!value) {
      return;
    }
    this.updateValue(value);
    this.shouldSetValue = true;
  }

  getValue() {
    return super.getValue();
  }

  /**
   * Called immediately after the component has been instantiated to initialize
   * the component.
   */
  init() {
    super.init();
  }

  /**
   * The second phase of component building where the component is rendered as an HTML string.
   *
   * @returns {string} - The return is the full string of the component
   */
  render() {
    // For react components, we simply render as a div which will become the react instance.
    // By calling super.render(string) it will wrap the component with the needed wrappers to make it a full component.
    return super.render(`<div ref="react-${this.id}"></div>`);
  }

  validate(data, dirty, rowData) {
    //return true
    return super.validate(data, dirty, rowData);
  }

  checkValidity(data, dirty, rowData) {
    super.checkValidity(data, dirty, rowData);
    if (this.error && this.error.message) {
      if (this.error.message === "Campo richiesto") {
        return false;
      } else {
        this.error.message = null;
        this.error.messages = null;
        return true;
      }
    } else {
      return true;
    }
    //return this.validate(data, dirty, rowData);
  }
}
