import editForm from "./DynamicCalendar.form";
import React, { useEffect, useRef, useState } from "react";
import { ReactComponent, Webform } from "@formio/react";
import { createRoot } from "react-dom/client";
import { DayPicker } from "react-day-picker";
import Countdown from "react-countdown";
import "react-day-picker/style.css";
import { it, de, enGB } from "date-fns/locale";
import moment from "moment";
import "moment/locale/it";
import "moment/locale/en-gb";
import "moment/locale/de";
import {
  Alert,
  Button,
  Icon,
  Input,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
} from "design-react-kit";
import {
  convertIntegerToTime,
  convertTimeInInteger,
} from "../../class/ConvertTimeInInteger";
import Swal from "sweetalert2";
import { baseUrl } from "../../_helpers/BaseUrl";
import { sessionGet } from "../../_helpers/utilities";
import DaySelected from "./DaySelected";
import { Formio } from "@formio/js";

export const ReactDynamicCalendar = ({
  component,
  current,
  onChange,
  i18next,
}) => {
  //Data sul calendario selezionata
  const [selected, setSelected] = useState();

  //Date sul calendario disabilitate
  const [disabledDates, setDisabledDates] = useState([]);

  const [daysSlot, setDaysSlot] = useState([]);
  // Mostra errore se non ci sono date disponibili
  const [noAvailableSlot, setNoAvailableSlot] = useState(false);

  //Meeting id restituito dalla chiamata draft
  const [draftMeeting, setDraftMeeting] = useState(null);

  // Slot selezionato
  const [slotSelected, setSlotSelected] = useState(null);

  const [selectedData, setSelectedData] = useState(null);
  const [activeSlot, setActiveSlot] = useState(null);

  //Prima data disponibile del mese
  const [firstAvailableSlot, setFirstAvailableSlot] = useState({});
  // Dato caledario sola lettura
  const [renderData, setRenderData] = useState(null);

  //Modale
  const [isOpen, toggleModal] = useState(false);

  //Lista meeting
  const [listMeeting, setListMeeting] = useState([]);

  // Dato componente sola lettura
  const [renderDataReadOnly, setRenderDataReadOnly] = useState(false);

  const refButtonSlot = useRef();

  const calendarID = component.calendarId;

  moment.locale(i18next.language);

  const calculateAvailableDates = (date) => {
    let formattedDate = moment(date).format("YYYY-MM-DD");
    fetch(getMonthlyAvailabilitiesUrl(formattedDate))
      .then((response) => response.json())
      .then((dateAvailables) => {
        let daysInMonth = getListDaysInMonth(date ? date : new Date());

        //Filtro solo i giorni disponibili
        const elements = dateAvailables.filter(
          (item) => item.available == true,
        );

        //Conto quandi gionri disponibili ci sono
        const countAllElmAvailable = elements.length;
        if (countAllElmAvailable === 0) {
          resetValue();
        }

        let calculateAvailableDates = [];
        daysInMonth.forEach((day) => {
          if (
            dateAvailables.some(
              (item) => item.date === day && item.available == true,
            )
          ) {
            calculateAvailableDates.push({ available: true, date: day });
          } else {
            calculateAvailableDates.push({ available: false, date: day });
          }
        });
        const dates = calculateAvailableDates.map((el) => {
          if (el.available === false) {
            return moment(el.date, "YYYY-MM-DD").toDate();
          }
        });
        setDisabledDates(dates);
        // Seleziono il primo giorno disponibile
        const firstDayAvailable = elements.length ? elements[0].date : date;

        getDaysSlot(firstDayAvailable);
      })
      .catch((error) => console.error(error));
  };

  const getDaysSlot = (date) => {
    setActiveSlot(null);
    setSelectedData(null);
    setDaysSlot([]);
    let formattedDate = moment(date).format("YYYY-MM-DD");

    fetch(getDailyAvailabilitiesUrl(formattedDate))
      .then((response) => response.json())
      .then((daysSlot) => {
        const elements = daysSlot.filter((item) => item.availability == true);
        setFirstAvailableSlot(elements[0]);
        if (elements.length > 0) {
          setDaysSlot(daysSlot);
          setNoAvailableSlot(false);
          //Seleziono la data testuale sulla colonna degli slots
          setSelectedData(moment(date).format("L"));
          //Seleziono la data sul calendario
          setSelected(moment(date, "YYYY-MM-DD").toDate());
        } else {
          //Seleziono la data sul calendario
          if (daysSlot.length > 0) {
            setSelected(moment(daysSlot[0].date, "YYYY-MM-DD").toDate());
          }
          setDaysSlot([]);
          resetValue();
        }
      });
  };

  const saveRenderData = () => {
    if (calendarID !== "" && calendarID != null) {
      if (!current.dataValue) {
        calculateAvailableDates(new Date());
      } else {
        if (component.multiple && Array.isArray(current.dataValue)) {
          if (current.dataValue.length > 0) {
            setRenderData(current.dataValue);
            let parseListMeeting = [];
            current.dataValue.map((item) => {
              parseListMeeting.push({
                ...item,
                date: moment(item.date, "YYYY-MM-DD").format("DD-MM-YYYY"),
              });
            });
            setListMeeting(parseListMeeting);
          }
        } else if (
          !component.multiple &&
          typeof current.dataValue === "object"
        ) {
          const renderData = {
            date: current.dataValue.date,
            slot: current.dataValue.slot,
          };
          const parseListMeeting = {
            ...current.dataValue,
            date: moment(current.dataValue.date, "YYYY-MM-DD").format(
              "DD-MM-YYYY",
            ),
          };
          setListMeeting([parseListMeeting]);
          setRenderData(renderData);
        } else {
          let explodedValue = current.dataValue
            .replace(")", "")
            .replace(" (", " @ ")
            .replace(/\//g, "-")
            .split(" @ ");
          let explodedCalendar = explodedValue[2].split("#");

          const renderData = {
            date: moment(explodedValue[0], "DD-MM-YYYY").format("YYYY-MM-DD"),
            slot: explodedValue[1],
          };

          if (component.multiple) {
            onChange([
              {
                date: explodedValue[0],
                slot: explodedValue[1],
                calendar_id: explodedCalendar[0],
                meeting_id: explodedCalendar[1],
                opening_hour_id:
                  explodedCalendar.length === 3 ? explodedCalendar[2] : "",
              },
            ]);
            setListMeeting([
              {
                date: explodedValue[0],
                slot: explodedValue[1],
                calendar_id: explodedCalendar[0],
                meeting_id: explodedCalendar[1],
                opening_hour_id:
                  explodedCalendar.length === 3 ? explodedCalendar[2] : "",
              },
            ]);
          } else {
            onChange({
              date: explodedValue[0],
              slot: explodedValue[1],
              calendar_id: explodedCalendar[0],
              meeting_id: explodedCalendar[1],
              opening_hour_id:
                explodedCalendar.length === 3 ? explodedCalendar[2] : "",
            });
            setListMeeting([
              {
                date: explodedValue[0],
                slot: explodedValue[1],
                calendar_id: explodedCalendar[0],
                meeting_id: explodedCalendar[1],
                opening_hour_id:
                  explodedCalendar.length === 3 ? explodedCalendar[2] : "",
              },
            ]);
          }
          setRenderData(renderData);
        }
      }
    }
  };

  useEffect(() => {
    window.FormioInstance?.formReady.then(() => {
      setTimeout(() => {
        saveRenderData();
      }, 1000);
    });
  }, []);

  const getListDaysInMonth = (date) => {
    let monthDate = moment(date, "YYYY-MM");
    let daysInMonth = monthDate.daysInMonth();
    let arrDays = [];

    while (daysInMonth) {
      let current = moment(date).date(daysInMonth);
      arrDays.push(current.format("YYYY-MM-DD"));
      daysInMonth--;
    }
    return arrDays;
  };

  const getMonthlyAvailabilitiesUrl = (date, available = false) => {
    let calendarID = component.calendarId,
      selectOpeningHours = component.select_opening_hours,
      openingHours = component.select_opening_hours
        ? component.opening_hours
        : [],
      location = window.location,
      explodedPath = location.pathname.split("/");

    let url = `${baseUrl()}/api/calendars/${calendarID}/availabilities`;

    let queryParameters = [
      `from_time=${moment(date).startOf("month").format("YYYY-MM-DD")}`,
      `to_time=${moment(date).endOf("month").format("YYYY-MM-DD")}`,
    ];
    if (available) {
      queryParameters.push(`available=${available}`);
    }
    // filter availabilities by selected opening-hours (this is mandatory in case of overlapped opening hours)
    if (selectOpeningHours && openingHours) {
      // Select specific opening hours
      queryParameters.push(`opening_hours=${openingHours.join()}`);
    }

    return `${url}?${queryParameters.join("&")}`;
  };

  const getDailyAvailabilitiesUrl = (date, available = false) => {
    let calendarID = component.calendarId,
      selectOpeningHours = component.select_opening_hours,
      openingHours = component.select_opening_hours
        ? component.opening_hours
        : [],
      location = window.location,
      explodedPath = location.pathname.split("/");

    const formattedDate = moment(date).format("YYYY-MM-DD");
    let url = `${baseUrl()}/api/calendars/${calendarID}/availabilities/${formattedDate}`;

    let queryParameters = [];
    if (available) {
      queryParameters.push(`available=${available}`);
    }
    //if (this.meeting) {
    // Exclude saved meeting from unavailabilities
    //   queryParameters.push(`exclude=${this.meeting}`);
    // }
    if (selectOpeningHours && openingHours) {
      // Select specific opening hours
      queryParameters.push(`opening_hours=${openingHours.join()}`);
    }

    if (queryParameters) {
      url = `${url}?${queryParameters.join("&")}`;
    }

    return url;
  };

  const onMonthChange = (data) => {
    calculateAvailableDates(data);
  };

  const onDayClick = (data) => {
    toggleModal(!isOpen);
    getDaysSlot(data);
  };

  const onClickSelectDate = (element) => {
    debugger;
    setActiveSlot(null);
    setTimeout(() => {
      if (element) {
        setSlotSelected(element.target.dataset);
        setActiveSlot(element.target.id);
      }
    }, 300);
  };

  const createOrUpdateMeeting = (valueSlot) => {
    setDraftMeeting(null);

    let url = `${baseUrl()}/it/meetings/new-draft`,
      dataMeet = {
        date: slotSelected?.date
          ? moment(slotSelected?.date).format("DD-MM-YYYY")
          : moment(valueSlot.target.dataset.date).format("DD-MM-YYYY"),
        slot:
          slotSelected?.value_start && slotSelected?.value_end
            ? slotSelected.value_start + "-" + slotSelected.value_end
            : valueSlot.target.dataset.slots,
        calendar: component.calendarId,
        opening_hour: slotSelected?.opening_hour
          ? slotSelected?.opening_hour
          : valueSlot.target.dataset.opening_hour,
        meeting: draftMeeting ? draftMeeting.id : "",
        first_available_date: firstAvailableSlot.date,
        first_available_start_time: firstAvailableSlot.start_time,
        first_available_end_time: firstAvailableSlot.end_time,
        first_availability_updated_at: moment().format(),
      };

    fetch(url, {
      method: "POST",
      body: JSON.stringify(dataMeet),
      headers: {
        "Content-Type": "application/json; charset=utf-8",
      },
      strictErrors: true,
    })
      .then((response) => {
        if (!response.ok) {
          throw new Error(i18next.t("Intervallo selezionato non disponibile!"));
        }
        return response.json();
      })
      .then((draftMeet) => {
        setDraftMeeting(draftMeet);
        const updateMeeting = {
          //...dataMeet,
          date: dataMeet.date,
          slot: dataMeet.slot,
          calendar_id: dataMeet.calendar[0],
          opening_hour_id: dataMeet.opening_hour || dataMeet.opening_hour_id,
          meeting_id: draftMeet.id,
        };
        setListMeeting([...listMeeting, updateMeeting]);
      })
      .catch((e) => {
        console.log(e);
        Swal.fire({
          title: "",
          html: "<p>" + e + "</p>",
          icon: "error",
          cancelButtonText: i18next.t("Chiudi"),
          showCancelButton: true,
          showConfirmButton: false,
        });
      });
  };

  const handleConfirmMeeting = () => {
    if (slotSelected) {
      //Creo l'appuntamento in bozza
      createOrUpdateMeeting();
      toggleModal(!isOpen);
    }
  };

  const Completionist = () => (
    <span>{i18next.t("Tempo prenotazione scaduto!")}</span>
  );

  // Renderer CountDown callback with condition
  const renderer = ({ minutes, seconds, completed }) => {
    if (completed) {
      // Render a completed state
      return <Completionist />;
    } else {
      // Render a countdown
      return (
        <div>
          <div>
            {" "}
            <span>
              {" "}
              <i>
                {i18next.t(
                  "Per confermare la prenotazione che ti abbiamo riservato procedi con l’invio di questa pratica e con il pagamento (se richiesto) di quanto previsto entro",
                )}
                :
              </i>{" "}
              <b>
                {minutes} {i18next.t("minuti")} {seconds} {i18next.t("secondi")}
              </b>
            </span>
          </div>
        </div>
      );
    }
  };

  const handleFromTime = (e) => {
    const currentValue = convertTimeInInteger(e.target.value);
    const valueMin = convertTimeInInteger(e.target.min);
    const valueMax = convertTimeInInteger(e.target.max);
    if (
      currentValue < valueMin ||
      currentValue > valueMax ||
      currentValue > convertTimeInInteger(slotSelected.end)
    ) {
      Swal.fire({
        title: "",
        html: `<p>${i18next.t("l'orario")} <b>${convertIntegerToTime(currentValue)}</b> ${i18next.t("non è valido")}</p>`,
        icon: "error",
        cancelButtonText: i18next.t("Chiudi"),
        showCancelButton: true,
        showConfirmButton: false,
      });
      setValueCalendar("");
    } else {
      setSlotSelected({ ...slotSelected, value_start: e.target.value });
    }
  };

  const handleToTime = (e) => {
    const currentValue = convertTimeInInteger(e.target.value);
    const valueMin = convertTimeInInteger(e.target.min);
    const valueMax = convertTimeInInteger(e.target.max);
    debugger;
    if (
      currentValue < valueMin ||
      currentValue > valueMax ||
      currentValue < convertTimeInInteger(slotSelected.start)
    ) {
      Swal.fire({
        title: "",
        html: `<p>${i18next.t("l'orario")} <b>${convertIntegerToTime(currentValue)}</b> ${i18next.t("non è valido")}</p>`,
        icon: "error",
        cancelButtonText: i18next.t("Chiudi"),
        showCancelButton: true,
        showConfirmButton: false,
      });
      setValueCalendar("");
    } else {
      setSlotSelected({ ...slotSelected, value_end: e.target.value });
    }
  };

  // Imposto il valore del calendario per formio
  /**
   * @param {string} value
   */
  const setValueCalendar = (value) => {
    if (value === null || value === "") {
      //Faccio reset del valore di formio
      onChange("");
    } else {
      if (Array.isArray(value) && value.length > 0) {
        let valueMeetings = [];
        listMeeting.map((meeting) => {
          valueMeetings.push({
            date: moment(meeting.date, "DD-MM-YYYY").format("YYYY-MM-DD"),
            slot: meeting.slot,
            calendar_id: meeting.calendar_id || meeting.calendar[0],
            meeting_id: meeting.meeting_id,
            opening_hour_id: meeting.opening_hour || meeting.opening_hour_id,
          });
        });
        if (component.multiple) {
          onChange(valueMeetings);
        } else {
          onChange(valueMeetings[0]);
        }
      } else {
        onChange("");
      }
    }
  };

  //Reset dei valori di prenotazione
  const resetValue = () => {
    setNoAvailableSlot(true);
    setDraftMeeting(null);
    setSelectedData(null);
    setValueCalendar(null);
  };

  // Recupero il locale per il comp. DayPicker
  const getCurrentLocale = () => {
    if (i18next.language === "it") {
      return it;
    } else if (i18next.language === "en") return enGB;
    else if (i18next.language === "de") return de;
  };

  useEffect(() => {
    setValueCalendar(listMeeting);
  }, [listMeeting]);

  useEffect(() => {
    if (
      document.querySelector(".formio-read-only") &&
      renderDataReadOnly === false
    ) {
      setRenderDataReadOnly(true);
      setTimeout(() => {
        saveRenderData();
      }, 1000);
    } else {
      setRenderDataReadOnly(false);
      setTimeout(() => {
        saveRenderData();
      }, 1000);
    }
  }, []);

  const removeItem = (index, meeting) => {
    debugger;
    let url = `${baseUrl()}/api/meetings/${meeting.meeting_id}`;

    fetch(url, {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json; charset=utf-8",
        Authorization: `Bearer ${sessionGet("auth-token")}`,
      },
      strictErrors: true,
    }).then((response) => {
      if (response && response.status > 400) {
        Swal.fire({
          title: "",
          html: `<p>${i18next.t("Ci sono stati degli errori durante la cancellazione dello slot")}</p>`,
          icon: "error",
          cancelButtonText: i18next.t("Chiudi"),
          showCancelButton: true,
          showConfirmButton: false,
        });
      } else {
        const newArray = [
          ...listMeeting.slice(0, index),
          ...listMeeting.slice(index + 1),
        ];
        setListMeeting(newArray);
      }
    });
  };

  return (
    <>
      {renderDataReadOnly && renderData ? (
        <div className="my-5 mx-2 d-print-block d-preview-calendar d-preview-calendar-none">
          <b>
            {Array.isArray(renderData) && renderData.length > 1
              ? i18next.t("Giorni selezionati per la prenotazione")
              : i18next.t("Giorno selezionato per la prenotazione")}
            :
          </b>
          <div className="mb-5">
            <div className="it-list-wrapper">
              <ul className="it-list">
                {Array.isArray(renderData) ? (
                  renderData.map((data) => (
                    <DaySelected
                      day={moment(data.date, "DD-MM-YYYY").format("DD/MM/YYYY")}
                      i18next={i18next}
                      slot={data.slot}
                    ></DaySelected>
                  ))
                ) : (
                  <DaySelected
                    day={moment(renderData.date, "DD-MM-YYYY").format(
                      "DD/MM/YYYY",
                    )}
                    i18next={i18next}
                    slot={renderData.slot}
                  ></DaySelected>
                )}
              </ul>
            </div>
          </div>
        </div>
      ) : (
        <div className="slot-calendar ">
          <div className="row">
            {component.multiple ? (
              <div className="col-12 d-print-none d-preview-calendar-none">
                <div className={"m-2"}>
                  <h6>{i18next.t(component.label)}</h6>
                  <DayPicker
                    mode="single"
                    selected={selected}
                    locale={getCurrentLocale()}
                    disabled={disabledDates}
                    onMonthChange={onMonthChange}
                    onDayClick={onDayClick}
                  />
                  <p>
                    {i18next.t(
                      "Seleziona un'altra data nel calendario per aggiungere una nuova prenotazione",
                    )}
                  </p>
                </div>
              </div>
            ) : listMeeting.length !== 1 ? (
              <div className="col-12 d-print-none d-preview-calendar-none">
                <div className={"m-2"}>
                  <h6>{i18next.t(component.label)}</h6>
                  <DayPicker
                    mode="single"
                    selected={selected}
                    locale={getCurrentLocale()}
                    disabled={disabledDates}
                    onMonthChange={onMonthChange}
                    onDayClick={onDayClick}
                  />
                  <p>
                    {" "}
                    {i18next.t(
                      "Seleziona un'altra data nel calendario per aggiungere una nuova prenotazione",
                    )}
                  </p>
                </div>
              </div>
            ) : null}
            <div className="col-12">
              {listMeeting.length ? (
                <div className={"row m-2"}>
                  <div className={"col-12 mb-4"}>
                    <b>
                      {listMeeting.length > 1
                        ? i18next.t("Giorni selezionati per la prenotazione")
                        : i18next.t("Giorno selezionato per la prenotazione")}
                      :{" "}
                    </b>
                    <div className="mb-5">
                      <div className="it-list-wrapper">
                        <ul className="it-list">
                          {listMeeting.map((meeting, index) => {
                            return (
                              <li key={"li" + index}>
                                <div className="list-item">
                                  <div className="it-right-zone">
                                    <span className="text pl-4">
                                      {meeting.date}{" "}
                                      {slotSelected || meeting.slot
                                        ? `${i18next.t("alle")} ${meeting.slot}`
                                        : ""}
                                    </span>
                                    <Icon
                                      icon={"it-delete"}
                                      onClick={() => removeItem(index, meeting)}
                                      role={"button"}
                                    />
                                  </div>
                                </div>
                              </li>
                            );
                          })}
                        </ul>
                      </div>
                    </div>
                    {draftMeeting ? (
                      <Alert
                        color={"info"}
                        defaultValue={"string"}
                        className={"d-print-none d-preview-calendar-none"}
                      >
                        <Countdown
                          date={draftMeeting.expiration_time}
                          renderer={renderer}
                        ></Countdown>
                      </Alert>
                    ) : null}
                  </div>
                </div>
              ) : null}
            </div>
          </div>
        </div>
      )}
      <Modal
        isOpen={isOpen}
        toggle={() => toggleModal(!isOpen)}
        labelledBy="daySelected"
        size={"lg"}
        centered
        contentClassName={"ocl"}
      >
        <ModalHeader toggle={() => toggleModal(!isOpen)} id="daySelected">
          {i18next.t("Seleziona una fascia oraria")}
        </ModalHeader>
        {!noAvailableSlot ? (
          <div className="title-xsmall u-grey-dark px-4">
            {" "}
            {i18next.t(
              "Seleziona una fascia oraria e, se necessario, utilizza l'apposito controllo per accorciare la durata della prenotazione. Poi clicca su Conferma",
            )}
          </div>
        ) : null}
        <ModalBody>
          <div className="row">
            {listMeeting.length >= 1 ? (
              <div>
                {selectedData && !noAvailableSlot ? (
                  <h6>
                    {i18next.t("Orari disponibili del")} {selectedData}
                  </h6>
                ) : null}
                <div className="calendar-last-selection col-12 pt-2 mb-5">
                  <small className="pl-2 mb-2 d-flex px-2">
                    <b>
                      <Icon
                        icon={"it-clock"}
                        className={"mr-1"}
                        size={"sm"}
                      ></Icon>{" "}
                      {i18next.t("Ultimo orario selezionato")}
                    </b>
                  </small>
                  <div className="col-6 px-2">
                    <button
                      type="button"
                      id={"slot-last-selection"}
                      onClick={onClickSelectDate}
                      tabIndex="1"
                      className={
                        activeSlot === "slot-last-selection"
                          ? "active btn btn-slot p-0"
                          : "btn btn-slot p-0"
                      }
                      // data-date={listMeeting[listMeeting.length - 1].date}
                      data-date={moment(selectedData, "DD/MM/YYYY").format(
                        "YYYY-MM-DD",
                      )}
                      data-slots={listMeeting[listMeeting.length - 1].slot}
                      data-opening_hour={
                        listMeeting[listMeeting.length - 1].opening_hour_id
                      }
                      data-start={
                        listMeeting[listMeeting.length - 1].slot.split("-")[0]
                      }
                      data-end={
                        listMeeting[listMeeting.length - 1].slot.split("-")[1]
                      }
                      data-value_start={
                        listMeeting[listMeeting.length - 1].slot.split("-")[0]
                      }
                      data-value_end={
                        listMeeting[listMeeting.length - 1].slot.split("-")[1]
                      }
                      disabled={noAvailableSlot}
                    >
                      {listMeeting[listMeeting.length - 1].slot}
                    </button>
                  </div>
                </div>
              </div>
            ) : null}
            <div>
              {selectedData && listMeeting.length >= 1 ? (
                <h6>{i18next.t("Seleziona altri orari")}</h6>
              ) : noAvailableSlot ? null : (
                <h6>
                  {i18next.t("Orari disponibili del")} {selectedData}
                </h6>
              )}
            </div>
            {daysSlot.map((slot, index) => (
              <div className="col-6" key={"slot-" + index}>
                <button
                  type="button"
                  id={"slot-" + index}
                  onClick={onClickSelectDate}
                  ref={refButtonSlot}
                  className={
                    activeSlot === "slot-" + index
                      ? "active btn btn-slot p-0"
                      : "btn btn-slot p-0"
                  }
                  tabIndex="1"
                  data-date={slot.date}
                  data-start={slot.start_time}
                  data-end={slot.end_time}
                  data-value_start={slot.start_time}
                  data-value_end={slot.end_time}
                  data-slots={slot.start_time + "-" + slot.end_time}
                  data-opening_hour={slot.opening_hour}
                  data-min_duration={slot.min_duration}
                  disabled={slot.slots_available == 0}
                >
                  {slot.start_time} - {slot.end_time}
                </button>
              </div>
            ))}
            {noAvailableSlot ? (
              <div className="callout callout-highlight warning">
                <div className="callout-title">
                  <Icon aria-hidden icon="it-help-circle" />
                  {i18next.t("Attenzione")}
                </div>
                <p>
                  {" "}
                  {i18next.t(
                    "Non è possibile prenotare per la data selezionata",
                  )}
                  .
                </p>
              </div>
            ) : null}

            <div className="col-12">
              {/*     <div
                                className="mt-3 d-print-block d-preview-calendar px-4 d-none">{selectedData ?
                                <div>
                                    <b>{i18next.t('Giorno selezionato per la prenotazione')}: </b> {selectedData} {slotSelected ? `${i18next.t('alle')} ${slotSelected.value_start}-${slotSelected.value_end}` : ''}
                                </div> : null}</div>*/}

              {slotSelected?.value_start && activeSlot ? (
                <div className={"row"}>
                  <div className={"col-12 mb-3"}>
                    <p className={"mb-3"}>
                      <b>
                        {i18next.t(
                          "Se necessario, utilizza il controllo che segue per accorciare la durata della prenotazione",
                        )}
                      </b>
                    </p>
                  </div>

                  <div className={"col-12 col-md-6 mb-4"}>
                    <Input
                      type="time"
                      label={i18next.t("Dalle")}
                      className="active"
                      defaultValue={
                        slotSelected.value_start || slotSelected.start
                      }
                      min={slotSelected.start}
                      max={slotSelected.end}
                      onBlur={handleFromTime}
                    />
                  </div>

                  <div className={"col-12 col-md-6 mb-4"}>
                    <Input
                      type="time"
                      label={i18next.t("alle")}
                      className="active"
                      defaultValue={slotSelected.value_end || slotSelected.end}
                      onBlur={handleToTime}
                      min={slotSelected.start}
                      max={slotSelected.end}
                    />
                  </div>
                </div>
              ) : null}
            </div>
          </div>
        </ModalBody>

        <ModalFooter>
          <Button
            color="primary"
            onClick={() => handleConfirmMeeting()}
            disabled={!selectedData || !slotSelected?.value_start}
          >
            {i18next.t("Conferma")}
          </Button>
        </ModalFooter>
      </Modal>
    </>
  );
};

export default class FormioDynamicCalendar extends ReactComponent {
  constructor(component, options, data) {
    super(component, options, data);
  }

  /**
   * This function tells the form builder about your component. It's name, icon and what group it should be in.
   *
   * @returns {{title: string, icon: string, group: string, documentation: string, weight: number, schema: *}}
   */
  static get builderInfo() {
    return {
      title: "Dynamic Calendar",
      group: "basic",
      icon: "bi bi-calendar-plus",
      documentation: "",
      weight: 70,
      schema: FormioDynamicCalendar.schema(),
    };
  }

  /**
   * This function is the default settings for the component. At a minimum you want to set the type to the registered
   * type of your component (i.e. when you call Components.setComponent('type', MyComponent) these types should match.
   *
   * @param sources
   * @returns {*}
   */
  static schema() {
    return ReactComponent.schema({
      type: "dynamic_calendar",
    });
  }

  /*
   * Defines the settingsForm when editing a component in the builder.
   */
  static editForm = editForm;

  /**
   * This function is called when the DIV has been rendered and added to the DOM. You can now instantiate the react component.
   *
   * @param DOMElement
   * #returns ReactInstance
   */
  attachReact(element, ref) {
    this.rootComponent = createRoot(element);
    this.rootComponent.render(
      <ReactDynamicCalendar
        ref={ref}
        component={this.component} // These are the component settings if you want to use them to render the component.
        value={this.dataValue} // The starting value of the component.
        onChange={(e) => this.setValue(e)} // The onChange event to call when the value changes.
        {...this}
        current={this}
      />,
    );
  }

  setValue(value) {
    if (!value) {
      return;
    }

    if (Array.isArray(value)) {
      this.selectedNextSlots = value;

      this.updateValue(value);
      this.shouldSetValue = true;
    } else if (typeof value === "object") {
      this.date = value.date;
      this.slot = value.slot;
      this.meeting = value.meeting_id;
      this.meeting_expiration_time = null;
      this.opening_hour = value.opening_hour_id;
      this.calendar_id = value.calendar_id;

      this.updateValue(value);
      this.shouldSetValue = true;
    } else {
      let explodedValue = value
        .replace(")", "")
        .replace(" (", " @ ")
        .replace(/\//g, "-")
        .split(" @ ");
      let explodedCalendar = explodedValue[2].split("#");

      this.date = explodedValue[0];
      this.slot = explodedValue[1];
      this.meeting = explodedCalendar[1];
      this.meeting_expiration_time = null;
      this.opening_hour =
        explodedCalendar.length === 3 ? explodedCalendar[2] : "";

      this.updateValue({
        date: this.date,
        slot: this.slot,
        calendar_id: this.calendar_id,
        meeting_id: this.meeting,
        opening_hour_id: this.opening_hour,
      });
    }
  }

  getValue() {
    if (
      !(this.date && this.slot && this.meeting && this.opening_hour) &&
      !(this.selectedNextSlots && this.selectedNextSlots.length > 0)
    ) {
      // Unset value (needed for calendars with 'required' option"
      return null;
    }
    let meeting_id = this.meeting ? this.meeting : "";
    let opening_hour = this.opening_hour ? this.opening_hour : "";

    if (this.component.multiple) {
      return this.selectedNextSlots;
    }

    return {
      date: this.date,
      slot: this.slot,
      calendar_id: this.calendar_id,
      meeting_id: meeting_id,
      opening_hour_id: opening_hour,
    };

    /*        return `${this.date.replace(/-/g, "/")} @ ${this.slot} (${
            this.component.calendarId
        }#${meeting_id}#${opening_hour})`;*/
    // return super.getValue();
  }

  /**
   * Called immediately after the component has been instantiated to initialize
   * the component.
   */
  init() {
    super.init();
  }

  /**
   * The second phase of component building where the component is rendered as an HTML string.
   *
   * @returns {string} - The return is the full string of the component
   */
  render() {
    // For react components, we simply render as a div which will become the react instance.
    // By calling super.render(string) it will wrap the component with the needed wrappers to make it a full component.
    return super.render(`<div ref="react-${this.id}"></div>`);
  }
}
