import React from "react";

function DaySelected({ day, slot, i18next }) {
  return (
    <li className="it-list-item">
      <div className="list-item">
        <div className="it-right-zone">
          <span className="text pl-4">
            <b>{day}</b> {i18next.t("alle")} {i18next.t("ore")} <b>{slot}</b>
          </span>
        </div>
      </div>
    </li>
  );
}

export default DaySelected;
