import { Components } from "@formio/react";
import DynamicCalendarEdit from "./DynamicCalendar.edit.display";
export default function (...extend) {
  return Components.baseEditForm(
    [
      {
        key: "display",
        components: DynamicCalendarEdit,
      },
    ],
    ...extend,
  );
}
