import { Components } from "@formio/react";
import AddressMapEdit from "./AddressMap.edit.display";
export default function (...extend) {
  return Components.baseEditForm(
    [
      {
        key: "display",
        components: AddressMapEdit,
      },
    ],
    ...extend,
  );
}
