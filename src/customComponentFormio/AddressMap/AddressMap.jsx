import editForm from "./AddressMap.form";
import React, { useEffect, useRef, useState } from "react";
import { ReactComponent } from "@formio/react";
import { createRoot } from "react-dom/client";
import {
  Button,
  Icon,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Progress,
} from "design-react-kit";
import { baseUrl } from "../../_helpers/BaseUrl";
import AccessibleAutocomplete from "../../components/AccessibleAutocomplete";
import L from "leaflet";
import "leaflet/dist/leaflet.css";
import _, { debounce } from "lodash";
import Map from "./Map";
import Swal from "sweetalert2";
import { locationNameFormatter } from "../../_helpers/utilities";

export const ReactAddressMap = ({
  component,
  value,
  current,
  onChange,
  i18next,
}) => {
  const [position, setPosition] = useState(null);
  const [optionSuggestions, setOptionSuggestions] = useState([]);
  const [boundingBox, setBoundingBox] = useState();
  const [areasLayer, setAreasLayer] = useState();
  // const [geographicAreas, setGeographicAreas] = useState();
  const [chechingMarkerPosition, setChechingMarkerPosition] = useState(false);
  const [geographicAreasIds, setGeographicAreasIds] = useState();
  const [loadingSuggestions, setLoadingSuggestions] = useState(false);
  const [allBounds, setAllBounds] = useState();
  const [modalMarkerPosition, setModalMarkerPosition] = useState();
  const [renderDataReadOnly, setRenderDataReadOnly] = useState(false);
  const [autocompleteValue, setAutocompleteValue] = useState("");
  const [isOpen, toggleModal] = useState(false);

  const queryParams = new URLSearchParams(window.location.search);
  const areaIdParam = queryParams.get("area");
  const { geographic_areas_id } = window.OC_SERVICE_DETAILS;

  const lastQuery = useRef();
  const language = i18next.language;
  let all_bounds = L.latLngBounds();

  //use areaId to filter geographicAreas by id
  const areaId = areaIdParam
    ? areaIdParam
    : geographic_areas_id && geographic_areas_id.length > 0
      ? geographic_areas_id
      : undefined;

  const fetchLocations = (query, populateResults) => {
    lastQuery.current = query;
    const apiUrl = `https://nominatim.openstreetmap.org/search?q=${encodeURIComponent(query)}&countrycodes=it&viewbox=${boundingBox}&bounded=1&addressdetails=1&limit=14&format=jsonv2&accept-language=${language}`;
    let currentSuggestions = [];
    let currentSuggestionsLabel = [];
    setLoadingSuggestions(true);
    fetch(apiUrl)
      .then((response) => response.json())
      .then((data) => {
        if (data.length && lastQuery.current === query) {
          setLoadingSuggestions(true);
          const fetchPromises = data.map((location, index) => {
            const { lat, lon } = location;
            const url = `${baseUrl()}/api/geographic-areas/contains?lat=${lat}&lon=${lon}&geographic_areas_ids=${geographicAreasIds.join(",")}`;
            return fetch(url)
              .then((areaResponse) => areaResponse.json())
              .then((areaData) => {
                if (areaData.result) {
                  location.label = locationNameFormatter(location);
                  return location; // Return the location object
                }
                return null; // Return null if the geographic check fails
              });
          });
          // Wait for all promises to resolve
          return Promise.all(fetchPromises);
        } else {
          // Handle no results from the initial API call
          if (lastQuery.current === query) {
            populateResults([]);
            setLoadingSuggestions(false);
          }
        }
      })
      .then((validLocations) => {
        validLocations = validLocations
          ? validLocations.filter((location) => location !== null)
          : [];
        if (validLocations.length > 0) {
          validLocations.forEach((location, index) => {
            validLocations[index].label = locationNameFormatter(location);
          });
          validLocations = _.uniqWith(
            validLocations,
            (a, c) => a.label === c.label,
          );
          validLocations.forEach((location, index) => {
            currentSuggestionsLabel.push(location.label);
            currentSuggestions.push(location);
          });
          if (lastQuery.current === query) {
            populateResults(currentSuggestionsLabel);
            setOptionSuggestions(currentSuggestions);
            setLoadingSuggestions(false);
          }
        } else {
          if (lastQuery.current === query) {
            populateResults([]);
            setLoadingSuggestions(false);
          }
        }
      })
      .catch((error) => {
        console.error("Error fetching locations:", error);
        setLoadingSuggestions(false);
      });
  };
  const suggest = (query, populateResults) => {
    fetchLocations(query, populateResults);
  };

  const fetchReverseGeocoding = (position, marker) => {
    const { lat, lng } = position;
    const url = `https://nominatim.openstreetmap.org/reverse?format=jsonv2&lat=${lat}&lon=${lng}&accept-language=${language}`;
    fetch(url)
      .then((response) => response.json())
      .then((data) => {
        if (data && data.display_name) {
          data.label = locationNameFormatter(data);
          data.lat = lat;
          data.lon = lng;
          marker.bindPopup(`${data.label}`).openPopup();
          setModalMarkerPosition(data);
        }
      })
      .catch((error) => console.error("Error with reverse geocoding:", error));
  };

  useEffect(() => {
    if (value) {
      setPosition(value);
    }
    if (current.valuePosition) {
      setPosition(current.valuePosition);
    }
  }, [value, current]);

  const saveRenderData = () => {
    if (value) {
      setPosition(value);
    }
    if (current.valuePosition) {
      setPosition(current.valuePosition);
    }
  };

  useEffect(() => {
    window?.Formio?.formioReady.then(() => {
      setTimeout(() => {
        console.log(component, current);
        saveRenderData();
        initComponent();
      }, 1);
    });
  }, []);

  const initComponent = () => {
    let baseUrlApi = baseUrl();
    let urlGeographicAreas = `${baseUrlApi}/api/geographic-areas`;
    let geographic_areas_ids = [];
    let geographic_areas = [];
    let areas_layer = [];
    fetch(urlGeographicAreas)
      .then((response) => response.json())
      .then((data) => {
        if (data) {
          data
            .filter((area) =>
              areaId
                ? Array.isArray(areaId)
                  ? areaId.includes(area.id)
                  : area.id === areaId
                : true,
            )
            .forEach((geo) => {
              geographic_areas_ids.push(geo.id);
              const geofence = JSON.parse(geo.geofence);
              geographic_areas.push(geofence);
              const geoJsonLayer = L.geoJSON(geofence, {
                style: {
                  color: "#3478bd",
                  fillColor: "#87CEEB",
                  fillOpacity: 0.2,
                  weight: 1.5,
                },
                filter: function (feature) {
                  return (
                    feature.geometry.type === "Polygon" ||
                    feature.geometry.type === "MultiPolygon"
                  );
                },
              });
              all_bounds.extend(geoJsonLayer.getBounds());
              areas_layer.push(geoJsonLayer);
            });
          setAreasLayer(areas_layer);
          // setGeographicAreas(geographic_areas)
          setGeographicAreasIds(geographic_areas_ids);
          setAllBounds(all_bounds);
          const bbNE = all_bounds.getNorthEast();
          const bbSW = all_bounds.getSouthWest();

          setBoundingBox([bbNE.lng, bbNE.lat, bbSW.lng, bbSW.lat]);
        } else {
          console.log("No geographic_areas_ids found.");
        }
      })
      .catch((error) => {
        console.error("Error with reverse geocoding:", error);
      });
  };

  const updateAutocompleteValue = () => {
    setAutocompleteValue(position?.label ? position.label : "");
  };

  useEffect(() => {
    updateAutocompleteValue();
    onChange(position);
  }, [position]);

  useEffect(() => {
    if (
      document.querySelector(".formio-read-only") &&
      renderDataReadOnly === false
    ) {
      setRenderDataReadOnly(true);
    } else {
      setRenderDataReadOnly(false);
    }
  }, []);

  const onSaveMapLocation = () => {
    if (modalMarkerPosition) {
      const url = `${baseUrl()}/api/geographic-areas/contains?lat=${modalMarkerPosition.lat}&lon=${modalMarkerPosition.lon}&geographic_areas_ids=${geographicAreasIds.join(",")}`;
      setChechingMarkerPosition(true);
      fetch(url)
        .then((response) => response.json())
        .then((data) => {
          setChechingMarkerPosition(false);
          if (data) {
            if (data.result === false) {
              Swal.fire({
                title: "",
                html:
                  "<p>" +
                  i18next.t(
                    "Attenzione: Il luogo selezionato si trova al di fuori dell’area coperta dal servizio. Per favore, scegli un luogo all'interno dell'area evidenziata nella mappa.",
                  ) +
                  "</p>",
                icon: "error",
                cancelButtonText: i18next.t("Chiudi"),
                showCancelButton: true,
                showConfirmButton: false,
              });
            } else if (data.result === true) {
              toggleModal(!isOpen);
              setPosition(modalMarkerPosition);
            }
          } else {
            setChechingMarkerPosition(false);
            console.log("No geographic-areas/contains .");
          }
        })
        .catch((error) => {
          console.error("Error with geographic-areas/contains:", error);
          Swal.fire({
            title: "",
            html: "<p>" + error + "</p>",
            icon: "error",
            cancelButtonText: i18next.t("Chiudi"),
            showCancelButton: true,
            showConfirmButton: false,
          });
        });
    }
  };

  return (
    <div>
      <div
        id={`address-map-container-${component.key}`}
        className="address-map-container"
      >
        <div>
          <Modal
            wrapClassName={"formio-component-address_map"}
            isOpen={isOpen}
            size={"lg"}
            scrollable
            toggle={() => toggleModal(!isOpen)}
            labelledBy="esempio2"
          >
            <ModalHeader toggle={() => toggleModal(!isOpen)} id="esempio2">
              {i18next.t("Individua il luogo sulla mappa")}
            </ModalHeader>
            <ModalBody>
              <p>
                <small>
                  {i18next.t(
                    "Trascina l’icona nel punto preciso in cui si verifica l’evento che vuoi segnalare e fai click su Salva indirizzo",
                  )}
                </small>
              </p>
              <Map
                position={position}
                areasLayer={areasLayer}
                bounds={allBounds}
                onMarkedMoved={(position, marker) => {
                  fetchReverseGeocoding(position, marker);
                }}
              />
            </ModalBody>
            <ModalFooter>
              <Button color="secondary" onClick={() => toggleModal(!isOpen)}>
                {i18next.t("Chiudi")}
              </Button>
              <Button
                color="primary"
                onClick={() => {
                  onSaveMapLocation();
                }}
                disabled={!modalMarkerPosition || chechingMarkerPosition}
              >
                {i18next.t("Salva indirizzo")}
              </Button>
            </ModalFooter>
          </Modal>
        </div>
        <div className="position-relative">
          <AccessibleAutocomplete
            id={`${component.id}-${component.key}`}
            inputClasses="form-control"
            source={debounce(suggest, 520)}
            onConfirm={(value) => {
              setLoadingSuggestions(false);
              lastQuery.current = "";
              if (value) {
                if (value === "") {
                  setPosition(null);
                }
                let pos = optionSuggestions.find(
                  (item) => item.label === value,
                );
                setPosition(pos);
              }
            }}
            onClear={() => {
              setLoadingSuggestions(false);
              lastQuery.current = "";
              setPosition(null);
            }}
            dropdownArrow={() => null}
            minLength={3}
            disabled={renderDataReadOnly}
            confirmOnBlur={true}
            showNoOptionsFound={!loadingSuggestions}
            defaultValue={position?.label}
            value={autocompleteValue}
            tNoResults={() =>
              i18next.t(
                "Nessun risultato trovato, prova a inserire l'indirizzo completo",
              )
            }
            tClearInput={() => i18next.t("Clear input")}
          />
          {loadingSuggestions && (
            <div
              className="position-absolute"
              style={{
                width: "100%",
                bottom: "0px",
              }}
            >
              <Progress indeterminate label="In elaborazione..." />
            </div>
          )}
        </div>
        {!renderDataReadOnly && (
          <Button
            // tag="input"
            type="button"
            value={i18next.t("Individua con precisione sulla mappa")}
            onClick={() => {
              toggleModal(true);
              setModalMarkerPosition(false);
            }}
            icon
            className="px-0 primary-color"
          >
            <Icon color="primary" icon="it-map-marker-circle" role={'presentation'}/>
            <span>{i18next.t("Individua con precisione sulla mappa")}</span>
          </Button>
        )}
      </div>
    </div>
  );
};

export default class FormioAddressMap extends ReactComponent {
  constructor(component, options, data) {
    super(component, options, data);
  }
  /**
   * This function tells the form builder about your component. It's name, icon and what group it should be in.
   *
   * @returns {{title: string, icon: string, group: string, documentation: string, weight: number, schema: *}}
   */
  static get builderInfo() {
    return {
      title: "Address Map",
      group: "basic",
      icon: "fa fa-map-location-dot",
      documentation: "",
      weight: 70,
      schema: FormioAddressMap.schema(),
    };
  }

  /**
   * This function is the default settings for the component. At a minimum you want to set the type to the registered
   * type of your component (i.e. when you call Components.setComponent('type', MyComponent) these types should match.
   *
   * @param sources
   * @returns {*}
   */
  static schema() {
    return ReactComponent.schema({
      type: "address_map",
    });
  }

  /*
   * Defines the settingsForm when editing a component in the builder.
   */
  static editForm = editForm;

  /**
   * This function is called when the DIV has been rendered and added to the DOM. You can now instantiate the react component.
   *
   * @param DOMElement
   * #returns ReactInstance
   */
  attachReact(element, ref) {
    this.rootComponent = createRoot(element);
    this.rootComponent.render(
      <ReactAddressMap
        ref={ref}
        key={this.dataValue}
        component={this.component} // These are the component settings if you want to use them to render the component.
        value={this.dataValue} // The starting value of the component.
        onChange={(e) => this.setValue(e)} // The onChange event to call when the value changes.
        {...this}
        current={this}
      />,
    );
  }

  setValue(value) {
    this.valuePosition = value;
    this.updateValue(value);
    
  }

  getValue() {
    return this.valuePosition;
    return super.getValue();
    // return super.getValue();
  }

  /**
   * Called immediately after the component has been instantiated to initialize
   * the component.
   */
  init() {
    super.init();
  }

  /**
   * The second phase of component building where the component is rendered as an HTML string.
   *
   * @returns {string} - The return is the full string of the component
   */
  render() {
    // For react components, we simply render as a div which will become the react instance.
    // By calling super.render(string) it will wrap the component with the needed wrappers to make it a full component.
    return super.render(
      `<div id="react-${this.id} ref="react-${this.id}"></div>`,
    );
  }
}
