import React, { useEffect, useRef, useState } from "react";
import "leaflet/dist/leaflet.css";
import Leaflet from "leaflet";
import L from "leaflet";
import { geoJSON, tileLayer } from "leaflet";

const Map = ({ position, areasLayer, bounds, onMarkedMoved }) => {
  const mapRef = useRef(null);
  let marker = null;
  const [polygon, setPolygon] = useState([]);
  const streetMaps = tileLayer(
    "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
    {
      // detectRetina: true,
      attribution:
        '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>',
    },
  );
  const defaultPosition = [41.9, 12.5];

  const initMap = () => {
    const map = Leaflet.map(mapRef.current, {
      keyboard: false,
      minZoom: 6,
      maxZoom: 17,
    }).setView(defaultPosition, 10);
    // map.scrollWheelZoom.disable()
    streetMaps.addTo(map);

    areasLayer &&
      areasLayer.map((layer) => {
        layer.addTo(map);
      });
    const centerMarker = position
      ? [`${position.lat}`, `${position.lon}`]
      : bounds
        ? bounds.getCenter()
        : defaultPosition;
    L.Icon.Default.mergeOptions({
      iconRetinaUrl: require("leaflet/dist/images/marker-icon-2x.png"),
      iconUrl: require("leaflet/dist/images/marker-icon.png"),
      shadowUrl: require("leaflet/dist/images/marker-shadow.png"),
    });
    marker = L.marker(centerMarker, { draggable: true }).addTo(map);

    marker.on("moveend", function (event) {
      const position = event.target.getLatLng();
      onMarkedMoved(position, marker);
    });
    if (!position && bounds) {
      map.fitBounds(bounds);
    } else {
      const newLatLng = position
        ? [position.lat, position.lon]
        : defaultPosition;
      if (marker) {
        marker.setLatLng(newLatLng).update();
        map.setView(newLatLng, 15);
      }
    }
  };

  useEffect(() => {
    if (mapRef.current) {
      initMap();
    }
  }, [mapRef]);
  return (
    <div
      className="d-block"
      style={{ height: "60vh", width: "100%", display: "block" }}
    >
      <div
        ref={mapRef}
        style={{ height: "100%", width: "100%" }}
        aria-hidden={true}
      />
    </div>
  );
};
export default Map;
