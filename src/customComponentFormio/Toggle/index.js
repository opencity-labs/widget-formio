import React, { Component } from "react";
import settingsForm from "./Toggle.settingsForm";
import { ReactComponent } from "@formio/react";
import ReactDOM from "react-dom";
import { createRoot } from "react-dom/client";

//const TextFieldComponent = Components.components.textfield;

/**
 * An example React component
 *
 * Replace this with your custom react component. It needs to have two things.
 * 1. The value should be stored is state as "value"
 * 2. When the value changes, call props.onChange(null, newValue);
 *
 * This component is very simple. When clicked, it will set its value to "Changed".
 */
const ReactCustomComp = class extends Component {
  constructor(props) {
    super(props);

    this.state = {
      value: props.value,
      label: props.component.label,
    };
  }

  setValue = (e) => {
    this.setState(
      (prevState) => ({ value: e.target.value }),
      () => this.props.onChange(null, this.state),
    );
  };

  render() {
    let { value } = this.state;
    value = value || "";
    const handleChange = (event) => {
      this.setValue(event);
      this.props.onChange(event.target.value);
    };

    return (
      <div className={"my-3"}>
        {/*        <div className="input-group">
          <input
              type="text"
              value={value}
              name="data[test]"
              placeholder={this.props.component.placeholder}
              className={"form-control "}
              onChange={this.setValue}
          />
        </div>*/}

        <div className="field">
          <input
            type="text"
            name="fullname"
            id="fullname"
            placeholder={this.props.component.placeholder}
            onChange={this.setValue}
            value={value}
          />
          <label htmlFor="fullname">{this.props.component.label}</label>
        </div>
      </div>
    );
  }
};

export default class Toggle extends ReactComponent {
  /**
   * This function tells the form builder about your component. It's name, icon and what group it should be in.
   *
   * @returns {{title: string, icon: string, group: string, documentation: string, weight: number, schema: *}}
   */
  static get builderInfo() {
    return {
      title: "React Component",
      icon: "square",
      group: "Data",
      documentation: "",
      weight: -10,
      schema: Toggle.schema(),
    };
  }

  /**
   * This function is the default settings for the component. At a minimum you want to set the type to the registered
   * type of your component (i.e. when you call Components.setComponent('type', MyComponent) these types should match.
   *
   * @param sources
   * @returns {*}
   */
  static schema() {
    return ReactComponent.schema({
      type: "toggle",
    });
  }

  /*
   * Defines the settingsForm when editing a component in the builder.
   */
  //static editForm = settingsForm;

  /**
   * This function is called when the DIV has been rendered and added to the DOM. You can now instantiate the react component.
   *
   * @param DOMElement
   * #returns ReactInstance
   */
  attachReact(element, ref) {
    this.rootComponent = createRoot(element);
    this.rootComponent.render(
      <ReactCustomComp
        ref={ref}
        component={this.component} // These are the component settings if you want to use them to render the component.
        value={this.dataValue} // The starting value of the component.
        onChange={this.updateValue} // The onChange event to call when the value changes.
        {...this}
      />,
    );
  }

  /**
   * Automatically detach any react components.
   *
   * @param element
   */
  detachReact(element) {
    if (element) {
      this.rootComponent.unmount();
    }
  }

  /**
   * Get the value of the component from the dom elements.
   *
   * @returns {Array}
   */
  getValue() {
    return this.dataValue;
  }

  setValue(value) {
    if (this.reactInstance) {
      this.reactInstance.setState({
        value: value,
      });
      this.shouldSetValue = false;
      this.dataValue = value;
    } else {
      this.shouldSetValue = true;
      this.dataForSetting = value;
    }
  }

  /**
   * The second phase of component building where the component is rendered as an HTML string.
   *
   * @returns {string} - The return is the full string of the component
   */
  render() {
    // For react components, we simply render as a div which will become the react instance.
    // By calling super.render(string) it will wrap the component with the needed wrappers to make it a full component.
    return super.render(`<div ref="react-${this.id}"></div>`);
  }
}
