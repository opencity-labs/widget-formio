const detectLanguageCustom = {
  name: "custom",

  lookup(options) {
    let found;
    if (typeof window !== "undefined") {
      const language = window.location.pathname.match(/^\/([\w]{2})\//g);
      if (language instanceof Array) {
        found = language[0].replace(/\//g, "");
      }
    }
    return found;
  },
};

export default detectLanguageCustom;
