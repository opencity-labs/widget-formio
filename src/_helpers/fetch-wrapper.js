import { getI18n } from "react-i18next";
import { authActions, store } from "../_store/index";

export const fetchWrapper = {
  get: request("GET"),
  getWithCredentials: requestWithCredentials("GET"),
  getGeoJson: request("GET", "application/geo+json"),
  post: request("POST"),
  patch: request("PATCH"),
  put: request("PUT"),
  putWithHeader: requestMultiPart("PUT", "putWithHeader"),
  delete: request("DELETE"),
};

function request(method, type) {
  return (url, body, excludeXLocale) => {
    const requestOptions = {
      method,
      headers: authHeader(url, type),
    };
    if (body) {
      requestOptions.headers["Content-Type"] = type ? type : "application/json";
      requestOptions.body = JSON.stringify(body);
    }

    if (!excludeXLocale) {
      requestOptions.headers["x-locale"] = getI18n().language || "it";
    }
    return fetch(url, requestOptions).then(handleResponse);
  };
}

function requestWithCredentials(method) {
  return (url, body) => {
    const requestOptions = {
      method,
      credentials: "include",
    };
    if (body) {
      requestOptions.headers["Content-Type"] = "application/json";
      requestOptions.body = JSON.stringify(body);
    }
    return fetch(url, requestOptions).then(handleResponse);
  };
}

function requestMultiPart(method, type) {
  return (url, body) => {
    const requestOptions = {
      method,
      headers: { "Content-Type": "multipart/form-data" },
    };
    if (body) {
      requestOptions.body = body;
    }

    return fetch(url, requestOptions).then((res) => {
      let etag;
      res.headers.forEach((val, key) => {
        etag = { etag: val.replace('/"', "").replace(/"/g, "") };
      });
      return { ...res.text(), etag };
    });
  };
}

function authHeader(url, type = "application/json") {
  // return auth header with jwt if user is logged in and request is to the api url
  const token = authToken();
  const isLoggedIn = !!token;
  const isApiUrl = url.includes("/api");

  if (isLoggedIn && isApiUrl) {
    return { Authorization: `Bearer ${token}`, Accept: type };
  } else {
    return { Accept: type };
  }
}

function authToken() {
  return store.getState().auth.token || `${process.env.REACT_APP_DEV_TOKEN}`;
}

function handleResponse(response) {
  return response.text().then((text) => {
    const data = text && JSON.parse(text);

    if (!response.ok) {
      if ([401, 403, 500].includes(response.status) && authToken()) {
        // auto logout if 401 Unauthorized or 403 Forbidden response returned from api
        const logout = () => store.dispatch(authActions.logout());
        logout();
      }

      const error =
        (data && data.message) ||
        (data && data.description) ||
        response.statusText;
      return Promise.reject(error);
    }

    return data;
  });
}
