export const baseUrl = () => {
  const location = window.location;
  const globalRegex = new RegExp(
    "qa.bugliano.pi.it|localhost|opencity-labs.gitlab.io",
    "g",
  );
  let url = null;
  if (globalRegex.test(location.hostname)) {
    url = "https://servizi.comune-qa.bugliano.pi.it/lang";
  } else if (window.BASE_URL) {
    url = window.BASE_URL;
  } else {
    const explodedPath = location.pathname.split("/");
    url = location.origin + "/" + explodedPath[1];
  }
  return url;
};
