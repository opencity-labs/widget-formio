import i18next from "i18next";

export function capitalizeFirstLetter(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}

export function formatBytes(bytes, decimals = 2) {
  if (!+bytes) return "0 Bytes";

  const k = 1024;
  const dm = decimals < 0 ? 0 : decimals;
  const sizes = ["Bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"];

  const i = Math.floor(Math.log(bytes) / Math.log(k));

  return `${parseFloat((bytes / Math.pow(k, i)).toFixed(dm))} ${sizes[i]}`;
}

export function resizeImage(base64Str, maxWidth = 400, maxHeight = 350) {
  return new Promise((resolve) => {
    let img = new Image();
    img.src = base64Str;
    img.onload = () => {
      let canvas = document.createElement("canvas");
      const MAX_WIDTH = maxWidth;
      const MAX_HEIGHT = maxHeight;
      let width = img.width;
      let height = img.height;
      let shouldResize = false;

      if (width > height) {
        if (width > MAX_WIDTH) {
          height *= MAX_WIDTH / width;
          width = MAX_WIDTH;
          shouldResize = true;
        }
      } else {
        if (height > MAX_HEIGHT) {
          width *= MAX_HEIGHT / height;
          height = MAX_HEIGHT;
          shouldResize = true;
        }
      }
      if (shouldResize) {
        canvas.width = width;
        canvas.height = height;
        let ctx = canvas.getContext("2d");
        ctx.drawImage(img, 0, 0, width, height);
        resolve(canvas.toDataURL("image/jpeg", 0.9));
      } else {
        resolve(base64Str);
      }
    };
  });
}

export function getBase64(file) {
  let reader = new FileReader();
  reader.readAsDataURL(file);
  reader.onload = function () {
    return reader.result;
  };
  reader.onerror = function (error) {
    console.log("Error: ", error);
  };
}

export function isImage(url) {
  return /\.(jpg|jpeg|png|webp|avif|gif|svg)$/.test(url);
}

export function countItemsNested(data) {
  let k = 0;
  let i = 0;
  const stats = {};
  data.forEach((arr) => {
    arr.attachments.map((e) =>
      Object.assign(stats, {
        images: isImage(e.name) ? (k = k + 1) : k,
        files: !isImage(e.name) ? (i = i + 1) : i,
      }),
    );
  });
  return stats;
}

export function detectBoundingBox() {
  // window.OC_BB = lng1,lon1,lng2,lon2
  const boundingBoxItaly =
    "6.7499552751,36.619987291,18.4802470232,47.1153931748";
  const boundingBoxCustom = window.OC_BB
    ? window.OC_BB.split(",")
    : boundingBoxItaly.split(",");
  return boundingBoxCustom
    ? [
        [boundingBoxCustom[1], boundingBoxCustom[0]],
        [boundingBoxCustom[3], boundingBoxCustom[2]],
      ]
    : [
        [6.7499552751, 36.619987291],
        [18.4802470232, 47.1153931748],
      ];
}

export function isObjectEmpty(objectName) {
  return (
    objectName &&
    Object.keys(objectName).length === 0 &&
    objectName.constructor === Object
  );
}

export const locationNameFormatter = (data) => {
  const properties = data.address;
  if (data.addresstype === "county" && properties["country"]) {
    return properties["county"];
  } else if (data.addresstype === "village" && properties["village"]) {
    return properties["village"];
  }
  const parts = [];
  const place = [
    "emergency",
    "historic",
    "military",
    "natural",
    "landuse",
    "place",
    "railway",
    "man_made",
    "aerialway",
    "boundary",
    "amenity",
    "aeroway",
    "club",
    "craft",
    "leisure",
    "office",
    "mountain_pass",
    "shop",
    "tourism",
    "bridge",
    "tunnel",
    "waterway",
    "name",
  ];

  place.forEach((prop) => {
    if (properties.hasOwnProperty(prop) && properties[prop]) {
      parts.push(properties[prop]);
    }
  });
  if (properties["road"]) {
    parts.push(properties["road"]);
  } else if (properties["pedestrian"]) {
    parts.push(properties["pedestrian"]);
  } else if (properties["suburb"]) {
    parts.push(properties["suburb"]);
  } else if (properties["square"]) {
    parts.push(properties["square"]);
  } else {
    if (properties["neighbourhood"]) {
      parts.push(properties["neighbourhood"]);
    }
    if (properties["hamlet"]) {
      parts.push(properties["hamlet"]);
    } else if (properties["isolated_dwelling"]) {
      parts.push(properties["isolated_dwelling"]);
    } else if (properties["locality"]) {
      parts.push(properties["locality"]);
    } else if (properties["croft"]) {
      parts.push(properties["croft"]);
    }
  }
  if (properties["house_number"]) {
    parts.push(properties["house_number"]);
  }
  if (properties["postcode"]) {
    parts.push(properties["postcode"]);
  }
  if (properties["village"]) {
    parts.push(properties["village"]);
  }
  if (properties["town"]) {
    parts.push(properties["town"]);
  } else if (properties["city"]) {
    parts.push(properties["city"]);
  }
  var name = parts.join(", ").substr(0, 150);
  return name;
};

export const checkAnonymUser = (email) => {
  const globalRegex = new RegExp("anonymous.fake.email", "gm");
  return globalRegex.test(email);
};

export const checkServiceNotScheduledNow = (service) => {
  let scheduled = service.status === 4;
  if (scheduled && service.scheduled_from && service.scheduled_to) {
    const nowTime = Date.now();
    scheduled =
      Date.parse(service.scheduled_from) > nowTime ||
      nowTime > Date.parse(service.scheduled_to)
        ? true
        : false;
  }
  return scheduled;
};

export const serviceHasStamps = (service) => {
  if (service?.stamps && service.stamps.some((s) => s.phase === "request")) {
    return true;
  } else {
    return false;
  }
};

// get from session (if the value expired it is destroyed)
export function sessionGet(key) {
  let stringValue = window.sessionStorage.getItem(key);
  if (stringValue !== null) {
    let value = JSON.parse(stringValue);
    let expirationDate = new Date(value.expirationDate);
    if (expirationDate > new Date()) {
      return value.value;
    } else {
      window.sessionStorage.removeItem(key);
    }
  }
  return null;
}

// add into session
export function sessionSet(key, value, expirationToken) {
  let expirationDate = expirationToken
    ? new Date(expirationToken * 1000)
    : null;
  if (expirationDate) {
    let newValue = {
      value: value,
      expirationDate: expirationDate.toISOString(),
    };
    window.sessionStorage.setItem(key, JSON.stringify(newValue));
  }
}

export function formatArrayWithLocale(words, locale) {
  if (!Array.isArray(words) || words.length === 0) {
    return "";
  }

  const separator = i18next.t("arraySeparator", { lng: locale }) || ", ";
  const lastSeparator =
    i18next.t("OrArrayLastSeparator", { lng: locale }) || " o ";

  if (words.length === 1) {
    return words[0];
  }
  const copy = [...words];
  const lastWord = copy.pop();
  return `${copy.join(separator)}${lastSeparator}${lastWord}`;
}

export function getLoginProviders(locale) {
  const loginProvider =
    window.OC_LOGIN_PROVIDER ||
    (process.env.REACT_APP_LOGIN_PROVIDER
      ? process.env.REACT_APP_LOGIN_PROVIDER.split(",")
      : []);
  return formatArrayWithLocale(loginProvider, locale);
}
