import { useContext, createContext, useState, useEffect } from "react";
import { LoginPage } from "../pages/LoginPage";
import { apiActions, authActions, currentUserActions } from "../_store";
import { useDispatch, useSelector } from "react-redux";
import { StatusService } from "../class/StatusService";
import {
  checkAnonymUser,
  checkServiceNotScheduledNow,
  sessionSet,
} from "../_helpers/utilities";
import { Spinner } from "design-react-kit";
import ErrorServiceStatus from "../components/ErrorServiceStatus";
import ErrorAuth from "../components/ErrorAuth";
import { jwtDecode } from "jwt-decode";
//import { useNavigate } from "react-router-dom";

const AuthContext = createContext();

const AuthProvider = ({ children, serviceId }) => {
  const [isAnonymUser, setIsAnonymUser] = useState(null);
  const [isAnonymUserAllowed, setIsAnonymUserAllowed] = useState(null);
  const [errorAuth, setErrorAuth] = useState(null);
  const [loading, setLoading] = useState(true);
  const [serviceStatus, setServiceStatus] = useState(true);
  const [serviceStatusDetails, setServiceStatusDetails] = useState(false);
  const dispatch = useDispatch();
  const setterWithoutLogin = (data) => {
    setIsAnonymUser(data);
    setErrorAuth(false);
  };

  const checkStateService = (idUser) => {
    dispatch(currentUserActions.getUserById(idUser))
      .unwrap()
      .then((user) => {
        const email = user ? user.email : null;
        console.log("email", email);
        console.log("user", user);
        dispatch(apiActions.getServiceById(serviceId))
          .unwrap()
          .then((service) => {
            setLoading(false);
            // save locally the service definition (formio components are outside of redux contex)
            window.OC_SERVICE_DETAILS = service;
            if (service.status === 2 || checkServiceNotScheduledNow(service)) {
              setServiceStatus(false);
              setServiceStatusDetails({
                status: service.status,
                scheduledFrom: service.scheduled_from,
                scheduledTo: service.scheduled_to,
              });
            } else if (
              checkAnonymUser(email) &&
              StatusService.STATUS_AVAILABLE
            ) {
              if (
                service.access_level === StatusService.ACCESS_LEVEL_ANONYMOUS
              ) {
                setIsAnonymUserAllowed(true);
              } else {
                setIsAnonymUserAllowed(false);
              }
            } else if (
              !checkAnonymUser(email) &&
              StatusService.STATUS_AVAILABLE
            ) {
              if (
                service.access_level === StatusService.ACCESS_LEVEL_SOCIAL ||
                service.access_level === StatusService.ACCESS_LEVEL_CIE ||
                service.access_level === StatusService.ACCESS_LEVEL_SPID_L1 ||
                service.access_level === StatusService.ACCESS_LEVEL_SPID_L2
              ) {
                setIsAnonymUser(false);
              } else if (
                service.access_level === StatusService.ACCESS_LEVEL_ANONYMOUS
              ) {
                setIsAnonymUser(false);
              }
            }
          })
          .catch((e) => {
            console.log(e);
          });
      });
  };

  const decodeUserToken = (token) => {
    return jwtDecode(token);
  };

  //Fake auth for developer mode use REACT_APP_DEV_TOKEN
  useEffect(() => {
    if (
      process.env.NODE_ENV === "development" &&
      `${process.env.REACT_APP_DEV_TOKEN}`
    ) {
      const user = decodeUserToken(`${process.env.REACT_APP_DEV_TOKEN}`);
      checkStateService(user.id);

      //save token for custom components formio
      sessionSet("auth-token", `${process.env.REACT_APP_DEV_TOKEN}`, user.exp);
    }
  }, [checkStateService]);

  useEffect(() => {
    console.log('process.env.NODE_ENV',process.env.NODE_ENV)
    if (serviceId && process.env.NODE_ENV === "production") {
      dispatch(authActions.getSessionToken()).then((el) => {
        if (el.meta.requestStatus !== "rejected") {
          const user = decodeUserToken(el.payload.token);
          checkStateService(user.id);

          //save token for custom components formio
          sessionSet(
            "auth-token",
            `${process.env.REACT_APP_DEV_TOKEN}`,
            user.exp,
          );
        } else {
          setErrorAuth(true);
          dispatch(authActions.createSessionToken())
            .unwrap()
            .then((res) => {
              const user = decodeUserToken(res.token);
              checkStateService(user.id);

              //save token for custom components formio
              sessionSet(
                "auth-token",
                `${process.env.REACT_APP_DEV_TOKEN}`,
                user.exp,
              );
            })
            .catch(() => {
              setLoading(false);
              setErrorAuth(true);
            });
        }
      });
    }
  }, [serviceId, dispatch]);
  return (
    <div className={"ocl"}>
      {loading ? (
        <div className="d-flex align-items-center justify-content-center vh-100">
          <Spinner active></Spinner>
        </div>
      ) : errorAuth === true ? (
        <LoginPage
          setterWithoutLogin={setterWithoutLogin}
          anonymAllowed={isAnonymUserAllowed}
        ></LoginPage>
      ) : serviceStatus === false ? (
        <ErrorServiceStatus serviceStatus={serviceStatusDetails} />
      ) : isAnonymUser === false ||
        (isAnonymUser === true && isAnonymUserAllowed === true) ? (
        <AuthContext.Provider value={{}}>{children}</AuthContext.Provider>
      ) : (
        <LoginPage setterWithoutLogin={setterWithoutLogin}></LoginPage>
      )}
    </div>
  );
};

export default AuthProvider;

export const useAuth = () => {
  return useContext(AuthContext);
};
