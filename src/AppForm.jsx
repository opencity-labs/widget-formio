import "typeface-titillium-web";
import "typeface-roboto-mono";
import "typeface-lora";

import {
  Button,
  Icon,
  NotificationManager,
  notify,
  Spinner,
  StepperContainer,
  StepperHeader,
  StepperHeaderElement,
  StepperNav,
} from "design-react-kit";
import { useEffect, useLayoutEffect, useRef, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { apiActions, formserverActions } from "./_store";
import Landing from "./pages/LandingPage/Landing";
import useScrollPercentage from "react-scroll-percentage-hook";
import InfoSteps from "./components/InfoSteps";
import i18next from "i18next";
import Stamps from "./components/Stamps";
import { getI18n, useTranslation } from "react-i18next";
import { checkAnonymUser, serviceHasStamps } from "./_helpers/utilities";
import { merge } from "lodash-es";
import dayjs from "dayjs";
import usePDND from "./hooks/usePDND";
import useOptionsFormio from "./hooks/useOptionsFormio";
import useProfileBlocks from "./hooks/useProfileBlocks";
import {Form, Utils} from "@formio/react";
import {disableApplicantField} from "./functions/disableApplicantField";
import {formCheckLabelRequired} from "./functions/formCheckLabelRequired";
import {removeDuplicatedIds} from "./functions/removeDuplicatedIds";

function App({
  submission,
  baseUrl,
  formserverUrl,
  signatureCheckWsUrl,
  serviceId,
}) {
  // Step generale corrente
  const [activeStep, toggleStep] = useState(0);

  // Step formio corrente
  const [stepFormIO, setStepFormIO] = useState(0);

  // Form  recuperato dal servizio
  const [formFormio, setFormFormio] = useState(null);

  // Form ID recuperato dal servizio
  const [formId, setFormId] = useState(null);

  // Lista dei flow steps
  const [flowSteps, setFlowSteps] = useState([]);

  // Submission aggiornata
  const [lastSubmission, setLastSubmission] = useState(null);
  //const [profileBlocks, setProfileBlocks] = useState({});
  const [isSettedProfileBlocks, setIsSettedProfileBlocks] = useState(false);
  // const [profileBlocksLoaded, setProfileBlocksLoaded] = useState(false)
  const [applicantDataLoaded, setApplicantDataLoaded] = useState(false);
  const [applicantData, setApplicantData] = useState({});
  // Controlla se il form è stato inviato
  const [submittedForm, setSubmittedForm] = useState(false);

  const [disabled, setDisabled] = useState([]);

  // Valore percentuale dello scroll della pagina
  const { percentage } = useScrollPercentage({ windowScroll: true });

  const dispatch = useDispatch();

  const { errors, service, application } = useSelector((x) => x.api);
  const { currentUser } = useSelector((x) => x.currentUser);
  //Hook traduzioni
  const { t } = useTranslation();

  // Lista opzioni di formio
  const [optionsFormio, optionsFormioReadOnly] = useOptionsFormio();

  const [stamps, setStamps] = useState([]);

  const [loading, setLoading] = useState(false);

  //Instanza formio
  let formInstance = useRef(null);

  const [dataPDND, getPDND] = usePDND(formInstance);
/*  const [profileBlocks, profileBlocksLoaded, getProfileBlocks] =
    useProfileBlocks(formInstance);*/

  // Azione step successivo generale
  const handleOnNextPage = () => {
    let steps = activeStep + 1;
    toggleStep(steps);
  };

  // Azione step precedente generale
  const handleOnPrevPage = () => {
    let steps = activeStep > 0 ? activeStep - 1 : 0;
    toggleStep(steps);
  };

  // Azione invio del form
  const handleSubmit = (callback) => {
    setLoading(true);
    formInstance.current
      .submit()
      .then(function () {
        const dataApplication = {
          //...(getId && { id: getId }),
          service_identifier: service?.id,
          service: service?.slug,
          ...lastSubmission,
          status: "1900",
        };
        console.log(dataApplication);
        applicationPOST(dataApplication, callback);
      })
      .catch((e) => {
        setLoading(false);
        console.log(e);
      });
  };

  const submitPayHandle = () => {
    handleSubmit(nextButtonHandle);
  };

  const applicationPOST = (data, callback) => {
    setLoading(true);
    dispatch(apiActions.createApplication(data))
      .unwrap()
      .then((response) => {
        if (callback) {
          callback();
        } else {
          setSubmittedForm(true);
        }
        setLoading(false);
      })
      .catch((err) => {
        notify("Errore", err.description || err.message, {
          dismissable: true,
          state: "error",
          duration: 10000,
        });
        setLoading(false);
      });
  };

  const applicationPatch = (data) => {
    dispatch(apiActions.patchApplication({ id: application.id, data: data }))
      .unwrap()
      .then((response) => {
        setSubmittedForm(true);
      })
      .catch((err) => {
        notify("Errore", err.description || err.message, {
          dismissable: true,
          state: "error",
          duration: 10000,
        });
      });
  };

  // Controllo se ci sono step nascosti e mostro solo quelli visibili
  const checkHiddenPanels = () => {
    if (flowSteps.length && flowSteps[activeStep].type === "formio") {
      calculateFormsStep(formInstance.current.root.establishPages());
    }
  };

  /*    const enableProfileBlocks = () =>{
            const profileBlockComponents = Utils.searchComponents(formInstance.current.components, {
                'component.properties.profile_block': 'true'
            });

            let checkProfileBlockUsed = []
            profileBlockComponents.forEach(element => {
                checkProfileBlockUsed.push(element.component.key)
            })

            if(checkProfileBlockUsed.length && profileBlockComponents.length){
                dispatch(apiActions.profileBlocks({userId:currentUser.id,keys:checkProfileBlockUsed.join()}))
                    .unwrap()
                    .then((profileBlocks) => {
                        let profileblocksData = {}
                        if(profileBlocks.data.length > 0){
                            profileBlocks.data.forEach(element => {
                                if(checkProfileBlockUsed.includes(element.key)){
                                    profileblocksData = {
                                        ...profileblocksData,
                                        [element.key]:{
                                            data : element.value.data
                                        }
                                    }
                                }
                            })
                            // setLastSubmission({data: profileblocksData})
                            setProfileBlocks({data: profileblocksData})
                            setProfileBlocksLoaded(true)

                        } else {
                            setProfileBlocksLoaded(true)
                        }
                    })
                    .catch(()=> {
                        setProfileBlocksLoaded(true)
                    })
            } else {
                setProfileBlocksLoaded(true)
            }

        }*/

  const getApplicantData = () => {
    if (checkAnonymUser(currentUser.email)) {
      setApplicantDataLoaded(true);
      return;
    }
    const applicantData = {
      data: {
        applicant: {
          data: {
            completename: {
              data: {
                name: currentUser.nome,
                surname: currentUser.cognome,
              },
            },
            fiscal_code: {
              data: { fiscal_code: currentUser.codice_fiscale },
            },
            Born: {
              data: {
                natoAIl: dayjs(currentUser.data_nascita)
                  .locale(getI18n().language)
                  .format("L"),
                place_of_birth: currentUser.luogo_nascita,
              },
            },
            email_address: currentUser.email ? currentUser.email : "",
            phone_number: currentUser.cellulare ? currentUser.cellulare : "",
          },
        },
      },
    };

    setApplicantData(applicantData);
    setApplicantDataLoaded(true);
    if (
      currentUser?.nome &&
      currentUser?.cognome &&
      currentUser?.codice_fiscale
    ) {
      setDisabled([
        "name",
        "surname",
        "fiscal_code",
        "natoAil",
        "place_of_birth",
      ]);
    }
  };

  useEffect(() => {
 /*   if (applicantDataLoaded && profileBlocksLoaded && !isSettedProfileBlocks) {
      console.log("lastSubmission", lastSubmission);
      console.log("applicantData", applicantData);
      console.log("profileBlocks", profileBlocks);
      const data = merge(lastSubmission, applicantData, profileBlocks);
      setLastSubmission(data);
      setIsSettedProfileBlocks(true);
    } else */if (applicantDataLoaded && applicantData) {

      const data = merge(lastSubmission, applicantData);
      if (data && formInstance.current) {
        formInstance.current.setValue(data);
        //disableApplicantField(formInstance.current)
      }
    }
  }, [applicantDataLoaded]);



  /*    useEffect( () => {
            console.log('last',lastSubmission)
            if(dataPDND){
                const pdnd = { data: {
                        [dataPDND.meta.format]: dataPDND
                    }
                }
                const data = merge(lastSubmission,pdnd)
                console.log('data1',data)
                setLastSubmission(data)
            }
        },[dataPDND])*/

  useEffect(() => {
    if (!checkAnonymUser(currentUser.email) && formInstance.current) {

      // getApplicantData()
      // getPDND()
      dispatch(apiActions.getSubmissionByServiceId({ serviceId: serviceId }))
        .unwrap()
        .then((submission) => {
          if (submission) {
            let newSubmission = {}
            Object.keys(submission).map(key => {
              const searchComponentInForm = Utils.searchComponents(formInstance.current.components, {
                key: key,
              });

              if (searchComponentInForm) {
                newSubmission = {
                  data: {
                    ...newSubmission.data,
                [key]: submission[key],
                  }
                }
              }
            })
            console.log('newSubmission',newSubmission)
            setApplicantData(newSubmission);
            setApplicantDataLoaded(true);
          }
        })
        .catch((err) => console.log(err));
    }
  }, [currentUser,formInstance.current]);

  const getFocusFirstElementButton = () => {
    setTimeout(() => {
      let firstFocusableElement = document.querySelector(
        "widget-formio button",
      );
      let alertErrorFocusableElement = document.querySelector(
        "widget-formio [role=link]",
      );
      if (firstFocusableElement) {
        if (firstFocusableElement) {
          firstFocusableElement.blur();
          firstFocusableElement.focus();
        }
      }
      if (alertErrorFocusableElement) {
        alertErrorFocusableElement.blur();
        alertErrorFocusableElement.focus();
      }
    }, 1000);
  };

  useEffect(() => {
    window.scrollTo(0, 0);
    getFocusFirstElementButton();
    // Al cambio pagina controllo se sono autenticato e disabilito i campi applicant
    if(!checkAnonymUser(currentUser.email)){
      if (formInstance && formInstance.current) {
        disableApplicantField(formInstance.current)
        setTimeout(() => {
          formCheckLabelRequired();removeDuplicatedIds();
        },1000)
      }
    }else{
      if (formInstance && formInstance.current) {
        setTimeout(() => {
          formCheckLabelRequired();removeDuplicatedIds();
        },1000)
      }
    }
  }, [activeStep]);

  useEffect(() => {
    if (service && baseUrl && formserverUrl) {
      setFormId(service.flow_steps[0].identifier);
      console.log(service.flow_steps[0].identifier);
      console.log(formId);
      dispatch(
        formserverActions.getFormById(
          formId || service.flow_steps[0].identifier,
        ),
      )
        .unwrap()
        .then((form) => {
          dispatch(
            formserverActions.getTranslationsFormById(
              formId || service.flow_steps[0].identifier,
            ),
          )
            .unwrap()
            .then((translations) => {
              //Aggiungo le traduzioni degli steps
              i18next.addResources(
                getI18n().language,
                "translation",
                translations[getI18n().language],
              );
              //i18next.addResourceBundle(getI18n().language, 'translation', translations[getI18n().language])


              //Calcolo i steps visibili
              const formSteps = formInstance?.current?.root
                ? formInstance?.current?.root.establishPages()
                : form.components;
              calculateFormsStep(formSteps);
            });
        })
        .catch((e) => {
          console.log(e);
        });
    }
  }, [service, formId, dispatch, baseUrl, formserverUrl]);

  useEffect(() => {
    flowSteps.map((f) => {
      if (f.type === "stamps") {
        const flowStepData = f.data.filter((d) => d.phase === "request");
        if (flowStepData) {
          const stampsData = [];
          flowStepData.map((s, i) => {
            stampsData[i] = { ...s, number: "", emitted_at: "" };
          });
          setStamps(stampsData);
        }
      }
    });
  }, [flowSteps]);

  const calculateFormsStep = (form) => {
    setFormFormio(form);
    let newFormSteps = form.map((el, i, row) => {
      return {
        title: el.title,
        type: "formio",
        last: i + 1 === row.length,
        hidden: false,
      };
    });
    let flowSteps = [
      ...newFormSteps,
      { title: t("riepilogo"), type: "summary", hidden: false },
    ];
    if (serviceHasStamps(service)) {
      flowSteps.push({
        title: t("bolli"),
        type: "stamps",
        hidden: false,
        data: service.stamps,
      });
    }
    setFlowSteps(flowSteps);
  };

  const saveApplication = () => {
    debugger
    const getId =
      application?.id || application?.data?.length
        ? application?.id || application?.data[0]?.id
        : null;
    const dataApplication = {
      ...(getId && { id: getId }),
      service: service?.identifier || service?.id,
      ...lastSubmission,
      status: '1000'
    };

    if (dataApplication?.id) {
      return dispatch(
        apiActions.updateApplication({
          data: dataApplication,
          id: dataApplication.id
        })
      ).then(() => {
        notify(t('Richiesta salvata con successo'), {
          dismissable: true,
          state: 'success',
          duration: 6000
        });
        dispatch(apiActions.getDraftApplication());
      });
    } else {
      return dispatch(apiActions.createApplication(dataApplication))
        .unwrap()
        .then(() => {
          notify(t('Richiesta salvata con successo'), {
            dismissable: true,
            state: 'success',
            duration: 6000
          });
        })
        .catch((e) => {
          notify(t('Si è verificato un errore'), e.message, {
            dismissable: true,
            state: 'error',
            duration: 6000
          });
        });
    }
  };

  const prevButtonHandle = () => {
    if (flowSteps[activeStep].type === "summary") {
      setTimeout(() => {
        formInstance.current.root.setPage(stepFormIO);
      }, 200);
      handleOnPrevPage();
    } else if (flowSteps[activeStep].type === "formio") {
      setTimeout(() => {
        formInstance.current.root.prevPage();
        handleOnPrevPage();
      }, 200);
    }
  };

  const nextButtonHandle = () => {
    if (flowSteps[activeStep + 1]?.type === "stamps") {
      handleOnNextPage();
    } else if (
      flowSteps[activeStep].type === "summary" ||
      flowSteps[activeStep].type === "stamps"
    ) {
      setTimeout(() => {
        formInstance.current.root.setPage(stepFormIO);
      }, 200);
      handleOnNextPage();
    } else if (flowSteps[activeStep].type === "formio") {
      setTimeout(() => {
        //Controllo la validità del formio
        if (
          formInstance.current.root.checkValidity(
            lastSubmission,
            true,
            null,
            true,
          )
        ) {
          //Se esite un'altro step formio imposto l'indice della prossima pagina
          if (formInstance.current.currentNextPage >= 0) {
            setStepFormIO(formInstance.current.currentNextPage);
          } else {
            //Imposto il form alla pagina corrente se dovrò ritornarnci
            setStepFormIO(formInstance.current.page);
          }
          if (!flowSteps[activeStep].last) {
            formInstance.current.root.nextPage();
          }
          // Vado allo step successivo
          handleOnNextPage();
        } else {
          // Se il form non è valido mostro gli errori
          formInstance.current.root.nextPage();
          //Imposto il focus del primo elemento utile
          getFocusFirstElementButton();
        }
      }, 200);
    }
  };

  const instanceFormio = (instance) => {
    // @ts-ignore
    // window.translationsFormio = merge({}, DefaultFormioTranslations(), translations)
    window.FormioInstance = instance;
    formInstance.current = instance;

    if (formInstance.current) {
      formInstance.current.nosubmit = true;
      //Popolo i campi profile blocks se ci sono
     // getProfileBlocks();
      setTimeout(() => {
        formCheckLabelRequired();
        removeDuplicatedIds();
      },1000)
      }
  };

  const onChange = (submission) => {
    //controllo le pagine visibili
    checkHiddenPanels();
    //Disabilito i campi applicant quando sono autenticato
    if(!checkAnonymUser(currentUser.email)){
      disableApplicantField(formInstance.current)
    }
    //Salvo l'ultima submission aggioranta
    setLastSubmission(formInstance.current.submission);
    console.log(submission)
  };

  return (
    <div>
      {loading && (
        <div className="container">
          <div className="row justify-content-center">
            <div className="col-12 col-lg-10">
              <div className="app-form-spinner d-flex align-items-center justify-content-center position-fixed">
                <Spinner active></Spinner>
              </div>
            </div>
          </div>
        </div>
      )}
      <NotificationManager />
      {!submittedForm ? (
        <div>
          {/* <div className="container" id="main-container">
                        <div className="row justify-content-center">
                            <div className="col-12 col-lg-10">
                                <div className="cmp-breadcrumbs" role="navigation">
                                    <nav className="breadcrumb-container" aria-label="breadcrumb">
                                        <ol className="breadcrumb p-0" data-element="breadcrumb">
                                            <li className="breadcrumb-item"><a href="#">Home</a><span
                                                className="separator">/</span></li>
                                            <li className="breadcrumb-item active" aria-current="page">{service?.name}
                                            </li>
                                        </ol>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>*/}
          {formId && flowSteps.length && !submittedForm && !errors ? (
            <div>
              <div className="container">
                <div className="row justify-content-center">
                  <div className="col-12 col-lg-10">
                    <div className="cmp-hero">
                      <section className="it-hero-wrapper bg-white align-items-start">
                        <div className="it-hero-text-wrapper pt-0 ps-0 pb-3 pb-lg-4">
                          <h1
                            className="text-black"
                            data-element={service?.name}
                            title={service?.name}
                          >
                            {service?.name}
                          </h1>
                        </div>
                      </section>
                    </div>
                  </div>
                  <div className="col-12 col">
                    <StepperContainer>
                      <StepperHeader>
                        {flowSteps.map((el, index) => (
                          <StepperHeaderElement
                            key={"step" + index}
                            variant={
                              activeStep === index ? "active" : "confirmed"
                            }
                          >
                            {t(el.name) || t(el.title)}
                            {Boolean(activeStep === index) &&
                              <span class="visually-hidden">{t('Attivo')}</span>
                            }
                            {Boolean(activeStep > index) && 
                              <>
                                <span class="visually-hidden">{t('Confermato')}</span>
                                <Icon className={'steppers-success'}  icon="it-check" role={'presentation'} />
                              </>
                            }
                          </StepperHeaderElement>
                        ))}
                        <StepperHeaderElement tag="span" variant="mobile" aria-hidden="true" >
                          {activeStep + 1}/{flowSteps.length}
                        </StepperHeaderElement>
                      </StepperHeader>
                    </StepperContainer>
                  </div>
                </div>
              </div>
              <div className={"container"}>
                <div className="row">
                  <div className="col-12 col-lg-3 d-lg-block mb-4 d-none">
                    {formInstance.current ? (
                      <InfoSteps
                        percentage={percentage}
                        flowSteps={flowSteps}
                        activeStep={activeStep}
                        formFormio={formFormio}
                        formInstance={formInstance.current}
                      ></InfoSteps>
                    ) : null}
                  </div>
                  <div className="col-12 col-lg-9 ">
                    <StepperContainer>
                      <div className={""}>
                        {flowSteps[activeStep].type === "formio" ? (
                          <div className={"m-lg-3 it-page-sections-container"}>
                            {/*     <render-form
                                                            url={`${window.FORMSERVER_URL || process.env.REACT_APP_FORMSERVER_URL}/form/${formId}`}
                                                            options={JSON.stringify(options)}
                                                            submission={lastSubmission ? JSON.stringify(lastSubmission) : JSON.stringify(submission)}
                                                        ></render-form>*/}
                            <Form
                              src={`${window.FORMSERVER_URL || process.env.REACT_APP_FORMSERVER_URL}/form/${formId}`}
                              options={optionsFormio(disabled)}
                              submission={
                                lastSubmission ? lastSubmission : submission
                              }
                              formReady={instanceFormio}
                              onChange={onChange}
                            />
                          </div>
                        ) : null}
                        {flowSteps[activeStep].type === "summary" ? (
                          <div
                            className={
                              "m-lg-3 it-page-sections-container h-100"
                            }
                          >
                            <Form
                              src={`${window.FORMSERVER_URL || process.env.REACT_APP_FORMSERVER_URL}/form/${formId}`}
                              options={optionsFormioReadOnly()}
                              submission={
                                lastSubmission ? lastSubmission : submission
                              }
                            />
                            {/*   <render-form
                                                            url={`${window.FORMSERVER_URL || process.env.REACT_APP_FORMSERVER_URL}/form/${formId}`}
                                                            options={JSON.stringify(optionsSummary)}
                                                            submission={lastSubmission ? lastSubmission : JSON.stringify(submission)}
                                                        ></render-form>*/}
                          </div>
                        ) : null}
                        {flowSteps[activeStep].type === "stamps" ? (
                          <div
                            className={
                              "m-lg-3 it-page-sections-container h-100"
                            }
                          >
                            <Stamps
                              stamps={stamps}
                              setStamps={setStamps}
                            ></Stamps>
                          </div>
                        ) : null}
                      </div>
                      {/*{JSON.stringify(flowSteps[activeStep])}*/}
                      <StepperNav>
                        {flowSteps[activeStep].type !== "stamps" ? (
                          <Button
                            className="steppers-btn-prev"
                            color="primary"
                            outline
                            size="sm"
                            onClick={() => prevButtonHandle()}
                            disabled={activeStep === 0}
                          >
                            <Icon color="primary" icon="it-chevron-left" />
                            {t("back")}
                          </Button>
                        ) : (
                          <Button
                            className="steppers-btn-next"
                            color="primary"
                            outline
                            size="sm"
                            onClick={() => applicationPatch(stamps)}
                          >
                            {t("send")}
                          </Button>
                        )}
                        {flowSteps[activeStep].type === "formio" ? (
                          <>
                          {/* {!checkAnonymUser(currentUser.email) && (
                            <Button
                              type="button"
                              outline
                              color="primary"
                              size={'sm'}
                              className="bg-white steppers-btn-save saveBtn"
                              data-focus-mouse="false"
                              onClick={() => saveApplication()}
                            >
                              <span className="text-button-sm t-primary">{t('Salva richiesta')}</span>
                            </Button>
                          )} */}
                          <Button
                            className="steppers-btn-next"
                            color="primary"
                            size="sm"
                            onClick={() => nextButtonHandle()}
                          >
                            {t("next")}
                            <Icon color="primary" icon="it-chevron-right" />
                          </Button>
                        </>) : null}
                        {flowSteps[activeStep]?.type === "summary" &&
                        flowSteps[activeStep + 1]?.type !== "stamps" ? (
                          <Button
                            className="steppers-btn-confirm"
                            color="primary"
                            size="sm"
                            onClick={() => handleSubmit()}
                          >
                            {t("send")}
                          </Button>
                        ) : null}
                        {flowSteps[activeStep].type === "summary" &&
                        flowSteps[activeStep + 1]?.type === "stamps" ? (
                          <Button
                            className="steppers-btn-next"
                            color="primary"
                            size="sm"
                            onClick={() => submitPayHandle()}
                          >
                            {t("payandsend")}
                            <Icon color="primary" icon="it-chevron-right" />
                          </Button>
                        ) : null}
                      </StepperNav>
                    </StepperContainer>
                  </div>
                </div>
              </div>
            </div>
          ) : (
            <div className="container">
              <div className="row justify-content-center">
                <div className="col-12 col-lg-10">
                  <div className="d-flex align-items-center justify-content-center vh-100">
                    <Spinner active></Spinner>
                  </div>
                </div>
              </div>
            </div>
          )}
        </div>
      ) : (
        <Landing></Landing>
      )}
    </div>
  );
}

export default App;
