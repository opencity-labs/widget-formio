import App from "./AppForm";
import { store } from "./_store";
import { Provider } from "react-redux";
import React, { useLayoutEffect } from "react";
import { Components } from "@formio/react";
import AuthProvider from "./context/AuthProvider";
import FormioHelper from "./class/FormioHelper";
import { Formio, Templates } from "@formio/js";
import bootstrapItalia from "@opencitylabs/bootstrap-italia/bootstrapItalia";
import "./i18n/i18nService/i18n";
import FormioPageBreak from "./customComponentFormio/PageBreak/PageBreak";
import FormioDynamicCalendar from "./customComponentFormio/DynamicCalendar/DynamicCalendar";
import SdcFile from "./customComponentFormio/FileSdc/SdcFileReact";
import FormioAddressMap from "./customComponentFormio/AddressMap/AddressMap";
Components.addComponent("pagebreak", FormioPageBreak);
Components.addComponent("dynamic_calendar", FormioDynamicCalendar);
Components.addComponent("sdcfile", SdcFile);
Components.addComponent("address_map", FormioAddressMap);

Formio.use(bootstrapItalia);
Templates.framework = bootstrapItalia;

// @ts-ignore
window.FormioHelper = new FormioHelper();

function ProviderApp({
  serviceId,
  submission,
  baseUrl,
  formserverUrl,
  pdndUrl,
  signatureCheckWsUrl,
}) {
  window.BASE_URL = baseUrl;
  window.FORMSERVER_URL = formserverUrl;
  window.PDND_URL = pdndUrl;

  useLayoutEffect(() => {
    // Importing files depending on env
    if (process.env.REACT_APP_STYLE === "true") {
      if(process.env.REACT_APP_THEMA==="amaranto"){
        import("./assets/styles/themes/amaranto/App-amaranto.scss");
      }else{
        import("./assets/styles/themes/App-bootstrap.scss");
      }
    } else {

      import("./assets/styles/App.scss");
    }
  }, []);

  return (
    <Provider store={store}>
      <AuthProvider serviceId={serviceId}>
        <App
          submission={submission}
          serviceId={serviceId}
          baseUrl={baseUrl}
          formserverUrl={formserverUrl}
          pdndUrl={pdndUrl}
          signatureCheckWsUrl={signatureCheckWsUrl}
        ></App>
      </AuthProvider>
    </Provider>
  );
}

export default ProviderApp;
