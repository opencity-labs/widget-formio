import { defaultFormioTranslations } from "../i18n/i18nService/defaultFormioTranslations";
import { getI18n, useTranslation } from "react-i18next";

const useOptionsFormio = () => {
  const { i18n } = useTranslation();

  const optionsFormio = () => {
    return {
      readOnly: false,
      noAlerts: false,
      hide: true,
      buttonSettings: {
        showPrevious: false,
        showNext: false,
        showSubmit: false,
        showCancel: true,
      },
      breadCrumbSettings: {
        clickable: false,
      },
      hideBreadCrumb: true,
      hideWizardNav: true,
      language: i18n.language,
      i18n: defaultFormioTranslations(),
      sanitizeConfig: {
        allowedAttrs: ["ref", "src", "url", "data-oembed-url"],
        allowedTags: ["oembed", "svg", "use"],
        addTags: ["oembed", "svg", "use"],
        addAttr: ["url", "data-oembed-url"],
      },
    };
  };

  const optionsFormioReadOnly = () => {
    return {
      readOnly: true,
      noAlerts: false,
      hide: true,
      flatten: true,
      buttonSettings: {
        showPrevious: false,
        showNext: false,
        showSubmit: false,
        showCancel: false,
      },
      breadCrumbSettings: {
        clickable: false,
      },
      hideBreadCrumb: true,
      language: getI18n().language,
      sanitizeConfig: {
        allowedAttrs: ["ref", "src", "url", "data-oembed-url"],
        allowedTags: ["oembed", "svg", "use"],
        addTags: ["oembed", "svg", "use"],
        addAttr: ["url", "data-oembed-url"],
      },
    };
  };

  return [optionsFormio, optionsFormioReadOnly];
};

export default useOptionsFormio;
