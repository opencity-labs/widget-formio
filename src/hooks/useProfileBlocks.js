import { useState } from "react";
import { apiActions } from "../_store";
import { Utils } from "@formio/js";
import { useDispatch, useSelector } from "react-redux";

const useProfileBlocks = (formInstance) => {
  const { currentUser } = useSelector((x) => x.currentUser);
  const dispatch = useDispatch();
  const [profileBlocksLoaded, setProfileBlocksLoaded] = useState(false);
  const [profileBlocks, setProfileBlocks] = useState({});

  const getProfileBlocks = () => {
    const profileBlockComponents = Utils.searchComponents(
      formInstance.current.components,
      {
        "component.properties.profile_block": "true",
      },
    );

    let checkProfileBlockUsed = [];
    profileBlockComponents.forEach((element) => {
      checkProfileBlockUsed.push(element.component.key);
    });

    if (checkProfileBlockUsed.length && profileBlockComponents.length) {
      dispatch(
        apiActions.profileBlocks({
          userId: currentUser.id,
          keys: checkProfileBlockUsed.join(),
        }),
      )
        .unwrap()
        .then((profileBlocks) => {
          let profileblocksData = {};
          if (profileBlocks.data.length > 0) {
            profileBlocks.data.forEach((element) => {
              if (checkProfileBlockUsed.includes(element.key)) {
                profileblocksData = {
                  ...profileblocksData,
                  [element.key]: {
                    data: element.value.data,
                  },
                };
              }
            });

            setProfileBlocks({ data: profileblocksData });
            setProfileBlocksLoaded(true);
          } else {
            setProfileBlocksLoaded(true);
          }
        })
        .catch(() => {
          setProfileBlocksLoaded(true);
        });
    } else {
      setProfileBlocksLoaded(true);
    }
  };

  return [profileBlocks, profileBlocksLoaded, getProfileBlocks];
};

export default useProfileBlocks;
