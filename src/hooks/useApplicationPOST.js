import { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";

const useApplicationPOST = (formInstance) => {
  const [data, setData] = useState(null);
  const { service } = useSelector((x) => x.api);
  const { currentUser } = useSelector((x) => x.currentUser);
  const dispatch = useDispatch();

  const fetch = () => {};

  return [data, fetch];
};

export default useApplicationPOST;
