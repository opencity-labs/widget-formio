import { useState } from "react";
import { apiActions } from "../_store";
import { Utils } from "@formio/js";
import { useDispatch, useSelector } from "react-redux";
import dayjs from "dayjs";
import { getI18n } from "react-i18next";

const usePDND = (formInstance) => {
  const [dataPDND, setDataPDND] = useState(null);
  const { service } = useSelector((x) => x.api);
  const { currentUser } = useSelector((x) => x.currentUser);
  const dispatch = useDispatch();

  const getProfileBlock = (configs) => {
    const profileBlockComponents = Utils.searchComponents(
      formInstance.current.components,
      {
        "component.properties.profile_block": "true",
      },
    );
    if (profileBlockComponents.length > 0) {
      profileBlockComponents.forEach((component) => {
        const eserviceId = component.component.properties.eservice;
        const format = component.component.properties.format || "default";
        const fiscalCode = currentUser.codice_fiscale;

        let callUrl = configs.call_url;
        callUrl += "&fiscal_code=" + fiscalCode + "&format=" + format;
        dispatch(apiActions.getAPIUrl(callUrl))
          .unwrap()
          .then((pdndData) => {
            console.log(pdndData);
            component.element.innerHTML += `<p class="mt-n4"><span class="text-success"><i class="fa fa-check-circle ba bi-check-circle"></i></span> Dati provenienti da ${pdndData.meta.source} aggiornati al ${dayjs(pdndData.meta.created_at).locale(getI18n().language).format("L")}</p>`;
          });
      });
    }
  };
  const getProfileBlocks = (configs) => {
    const profileBlockComponents = Utils.searchComponents(
      formInstance.current.components,
      {
        "component.properties.profile_block": "true",
      },
    );
    if (profileBlockComponents.length > 0) {
      profileBlockComponents.forEach((component) => {
        const eserviceId = component.component.properties.eservice;
        const format = component.component.properties.format || "default";

        const configUsed = configs.data.filter(
          (config) => config.eservice_id === eserviceId,
        );
        const fiscalCode = currentUser.codice_fiscale;
        if (configUsed.length) {
          let callUrl = configUsed[2].call_url;

          callUrl += "&fiscal_code=" + fiscalCode + "&format=" + format;
          dispatch(apiActions.getAPIUrl(callUrl))
            .unwrap()
            .then((pdndData) => {
              console.log(component);
              //component.setValue(pdndData);
              setDataPDND(pdndData);

              console.log(pdndData);
              component.element.innerHTML += `<p class="mt-n4"><span class="text-success"><i class="fa fa-check-circle ba bi-check-circle"></i></span> Dati provenienti da ${pdndData.meta.source} aggiornati al ${dayjs(pdndData.meta.created_at).locale(getI18n().language).format("L")}</p>`;
            });
        }
      });
    }
  };

  const getPDND = () => {
    dispatch(apiActions.tenantInfo())
      .unwrap()
      .then((tenant) => {
        console.log(tenant);
        if (service.pdnd_config_ids.length) {
          console.log("dd", service.pdnd_config_ids);
          dispatch(
            apiActions.pdndConfigById({
              tenantId: tenant.id,
              configId: service.pdnd_config_ids[0],
            }),
          )
            .unwrap()
            .then((configs) => {
              console.log("configs", configs);
              getProfileBlock(configs);
            });
        } else {
          dispatch(apiActions.pdndConfig(tenant.id))
            .unwrap()
            .then((configs) => {
              console.log("configs", configs);
              getProfileBlocks(configs);
            });
        }
      });
  };

  return [dataPDND, getPDND];
};

export default usePDND;
