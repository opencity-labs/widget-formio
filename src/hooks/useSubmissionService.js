import { apiActions } from "../_store";
import { useDispatch } from "react-redux";
import { Utils } from "@formio/js";
import { useState } from "react";

const useSubmissionService = (serviceId, formInstance) => {
  const dispatch = useDispatch();
  const [verifiedData, setVerifiedData] = useState(null);

  const getSubmissionService = () => {
    /*   const applicantComponents = Utils.searchComponents(formInstance.current.components, {
            'key': 'applicant'
        });*/
    dispatch(apiActions.getSubmissionByServiceId({ serviceId: serviceId }))
      .unwrap()
      .then((submission) => {
        setVerifiedData(submission);
      })
      .catch(() => {});
  };

  return [getSubmissionService, verifiedData];
};

export default useSubmissionService;
