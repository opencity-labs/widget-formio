import TitleLanding from "./Title";
import FinalIndications from "./FinalIndications";
import { t } from "i18next";
import {
  getApplicationEmail,
  getSubTitleKey,
  getTitleKey,
} from "../../class/LandingPageCustom";

function BuiltIn({ application, service }) {
  const titleKey = getTitleKey(service.slug);
  const subTitleKey = getSubTitleKey(service.slug);
  const email = getApplicationEmail(application);

  return (
    <div className="cmp-heading p-0">
      <div className="d-flex align-items-center">
        <TitleLanding customTitleKey={titleKey} />
      </div>
      <div className="hero-text">
        <p>{t(subTitleKey)}.</p>
        {application?.data?.email ? (
          <p className="pt-3 pt-lg-4">
            {t("Abbiamo inviato il riepilogo all’email")}:<br />
            <strong>{application?.data?.email}</strong>
          </p>
        ) : null}
      </div>
      <div className={"mt-5"}>
        <FinalIndications service={service}></FinalIndications>
      </div>
    </div>
  );
}

export default BuiltIn;
