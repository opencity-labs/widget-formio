import TitleLanding from "./Title";
import FinalIndications from "./FinalIndications";
import { t } from "i18next";
import { getApplicationEmail } from "../../class/LandingPageCustom";

function General({ application, service }) {
  const email = getApplicationEmail(application);

  const handleNewPratice = () => {
   location.reload()
  }

  return (
    <div className="cmp-heading p-0">
      <div className="d-flex align-items-center">
        <TitleLanding></TitleLanding>
      </div>
      <div className="hero-text">
        <p>
          {t("Grazie, abbiamo ricevuto la tua pratica per")}{" "}
          <strong>
            {service?.name} ({t("richiesta n.")} {application?.id})
          </strong>
        </p>
        {email && (
          <p className="pt-3 pt-lg-4">
            {t("Abbiamo inviato il riepilogo all’email")}:<br />
            <strong>{email}</strong>
          </p>
        )}
        <p className="mt-4">
          Consulta i dettagli della pratica nella tua{" "}
          <a
            className="t-primary"
            href={`${window.BASE_URL}/pratiche/${application?.id}/detail`}
          >
            {t("area personale")}
          </a>{" "}
          {t("oppure presenta una")}{" "}
          <span className="text-decoration-underline text-primary cursor-pointer" onClick={() => handleNewPratice()}>
            {t("nuova pratica")}
          </span>
        </p>
      </div>
      <div className={"mt-5"}>
        <FinalIndications service={service}></FinalIndications>
      </div>
    </div>
  );
}

export default General;
