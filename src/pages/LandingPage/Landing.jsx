import { useSelector } from "react-redux";
import { isEmpty } from "lodash-es";
import { useEffect, useState } from "react";
import { LandingType } from "../../components/LandingType";
function Landing() {
  const { application, service } = useSelector((x) => x.api);

  const [loadedPage, setLoadedPage] = useState(true);

  useEffect(() => {
    if (!isEmpty(service) && service) {
      setLoadedPage(false);
    }
  }, [service]);

  return (
    <div className="container">
      <div className="row justify-content-center landing">
        <div className="col-12 col-lg-10">
          {!loadedPage ? (
            <LandingType
              service={service}
              application={application}
            ></LandingType>
          ) : null}
        </div>
      </div>
    </div>
  );
}

export default Landing;
