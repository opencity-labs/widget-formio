import TitleLanding from "./Title";
import FinalIndications from "./FinalIndications";
import { t } from "i18next";
import {
  getApplicationEmail,
  getSubTitleKey,
  getTitleKey,
} from "../../class/LandingPageCustom";

function Inefficiencies({ application, service }) {
  const titleKey = t("Segnalazione inviata");
  const subTitleKey = t("Grazie, abbiamo ricevuto la tua segnalazione");
  const email = getApplicationEmail(application);

  return (
    <div className="cmp-heading p-0">
      <div className="d-flex align-items-center">
        <TitleLanding customTitleKey={titleKey} />
      </div>
      <div className="hero-text">
        <p>
          {t(subTitleKey)}{" "}
          <strong>{application?.id ? application?.id : ""}</strong>
        </p>
        {email ? (
          <p className="pt-3 pt-lg-4">
            {t("Abbiamo inviato il riepilogo all’email")}:<br />
            <strong>{email}</strong>
          </p>
        ) : null}
      </div>
      <div className={"mt-5"}>
        <FinalIndications service={service}></FinalIndications>
      </div>
    </div>
  );
}

export default Inefficiencies;
