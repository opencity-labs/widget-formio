import TitleLanding from "./Title";
import FinalIndications from "./FinalIndications";
import { getApplicationEmail } from "../../class/LandingPageCustom";

function F24({ application, service }) {
  const email = getApplicationEmail(application);

  return (
    <div className="cmp-heading p-0">
      <div className="d-flex align-items-center">
        <TitleLanding></TitleLanding>
      </div>
      <div className="hero-text">
        <p>
          Il modello F24 n. <strong>{application?.id}</strong> per il servizio{" "}
          {service?.name} è stato generato.
        </p>
        {email && (
          <p className="pt-3 pt-lg-4">
            Abbiamo inviato il riepilogo all’email:
            <br />
            <strong>{email}</strong>
          </p>
        )}
        <p className="mt-4 mt-lg-3">
          <a
            className="t-primary"
            href={`${window.BASE_URL}/${application?.id}/detail`}
          >
            Consulta i dati inseriti
          </a>{" "}
          nella tua area riservata.
        </p>
      </div>
      <div className={"mt-5"}>
        <FinalIndications service={service}></FinalIndications>
      </div>
    </div>
  );
}

export default F24;
