import { Icon } from "design-react-kit";
import { t } from "i18next";

function TitleLanding({ customTitleKey }) {
  return (
    <div className="d-flex align-items-center">
      <Icon
        icon={"it-check-circle"}
        className="icon icon-success me-2 icon-sm mb-1"
      ></Icon>
      <h1 className="title-xxxlarge">
        {t(`${customTitleKey ? customTitleKey : "Pratica inviata"}`)}
      </h1>
    </div>
  );
}

export default TitleLanding;
