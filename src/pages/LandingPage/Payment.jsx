import TitleLanding from "./Title";
import FinalIndications from "./FinalIndications";
import { t } from "i18next";
import { getApplicationEmail } from "../../class/LandingPageCustom";

function Payment({ application, service }) {
  const email = getApplicationEmail(application);

  return (
    <div className="cmp-heading p-0">
      <div className="d-flex align-items-center">
        <TitleLanding></TitleLanding>
      </div>
      <div className="hero-text">
        <p>
          {service?.name}{" "}
          {t("è stata inviata con successo, sarai ricontattato presto")}.
        </p>
        <p className="pt-3 pt-lg-4">
          {t("Abbiamo inviato il riepilogo all’email")}:<br />
          <strong>{email}</strong>
        </p>
      </div>
      <div className={"mt-5"}>
        <FinalIndications service={service}></FinalIndications>
      </div>
    </div>
  );
}

export default Payment;
