import { Alert } from "design-react-kit";

function FinalIndications({ service }) {
  return (
    <Alert color="info" className="d-flex align-items-center">
      <div dangerouslySetInnerHTML={{ __html: service?.final_indications }} />
    </Alert>
  );
}

export default FinalIndications;
