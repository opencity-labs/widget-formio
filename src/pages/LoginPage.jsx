import { Button, Container, Row } from "design-react-kit";
import { t } from "i18next";
import { useTranslation } from "react-i18next";
import React from "react";
import { baseUrl } from "../_store";
import { getLoginProviders } from "../_helpers/utilities";

export { LoginPage };

function LoginPage({ setterWithoutLogin, anonymAllowed }) {
  const handleClick = () => {
    const urlParams = new URL(decodeURIComponent(window.location.href));
    const params = urlParams.toString().split("return-url=");
    const returnUrl = params[1];

    console.log(
      `${baseUrl() + "/login"}?return-url=${returnUrl || window.location.href}`,
    );
    window.location.href = `${baseUrl() + "/login"}?return-url=${returnUrl || window.location.href}`;
    return null;
  };

  const handleClickAnonym = () => {
    setterWithoutLogin(true);
  };

  const { i18n } = useTranslation();
  const language = i18n.language;
  const loginProvider = getLoginProviders(language);

  return (
    <div className={"ocl"}>
      <Container style={{ minHeight: "408px" }}>
        <Row>
          <div className="col-12 col-lg-10 offset-lg-1">
            <div className="cmp-heading pb-3 pb-lg-4">
              <h1 className="title-xxxlarge">
                {t("login", { loginProviders: loginProvider })}
              </h1>
              <p className="subtitle-small">
                {t("login_description", { loginProviders: loginProvider })}
              </p>
            </div>
          </div>
        </Row>
        <hr className="d-none d-lg-block mt-0 mb-4" />
        <Row>
          <div className="cmp-text-button col-lg-6">
            <h2 className="title-xxlarge mb-0">
              {t("id_login_auth", { loginProviders: loginProvider })}
            </h2>
            <div className="text-wrapper">
              <p className="subtitle-small mb-3">
                {t("id_description", { loginProviders: loginProvider })}
              </p>
            </div>
            <div className="button-wrapper mb-2">
              <Button
                type="button"
                color={"primary"}
                className="btn-icon"
                onClick={handleClick}
              >
                <span className="">{t("login")}</span>
              </Button>
            </div>
          </div>

          {anonymAllowed && (
            <div className="cmp-text-button col-lg-6">
              <h2 className="title-xxlarge mb-0">{t("access_anonymous")}</h2>
              <div className="text-wrapper">
                <p className="subtitle-small mb-3">
                  {t("access_anonymous_description")}
                </p>
              </div>
              <div className="button-wrapper mb-2">
                <Button
                  type="button"
                  color={"primary"}
                  className="btn-icon"
                  onClick={handleClickAnonym}
                >
                  <span>{t("access_anonymous")}</span>
                </Button>
              </div>
            </div>
          )}
        </Row>
      </Container>
    </div>
  );
}
