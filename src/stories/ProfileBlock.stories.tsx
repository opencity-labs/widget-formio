import type { Meta, StoryObj } from "@storybook/react";
import Widget from "./Widget";
import { Provider } from "react-redux";
import { store } from "../_store";

const meta = {
  title: "Example/Profile Blocks",
  component: Widget,
  decorators: [(story) => <Provider store={store}>{story()}</Provider>],
  tags: ["autodocs"],
  parameters: {
    // More on how to position stories at: https://storybook.js.org/docs/configure/story-layout
    layout: "fullscreen",
  },
} satisfies Meta<typeof Widget>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Servizi: Story = {
  args: {
    serviceId: "f6882164-f00e-4a80-88ce-d40f7d0ffb73",
    baseUrl: "https://servizi.comune-qa.bugliano.pi.it/lang",
    formserverUrl: "https://form-qa.stanzadelcittadino.it",
  },
};

// More on interaction testing: https://storybook.js.org/docs/writing-tests/interaction-testing
