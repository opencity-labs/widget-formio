import type { Meta, StoryObj } from "@storybook/react";
import Widget from "./Widget";
import { Provider } from "react-redux";
import { store } from "../_store";

const meta = {
  title: "Example/Richiesta assistenza",
  component: Widget,
  decorators: [(story) => <Provider store={store}>{story()}</Provider>],
  tags: ["autodocs"],
  parameters: {
    // More on how to position stories at: https://storybook.js.org/docs/configure/story-layout
    layout: "fullscreen",
  },
} satisfies Meta<typeof Widget>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Servizi: Story = {
  args: {
    serviceId: "7c01b9ac-63a3-4b32-b822-68a74ca40ee0",
    baseUrl: "https://servizi.comune-qa.bugliano.pi.it/lang",
    formserverUrl: "https://form-qa.stanzadelcittadino.it",
  },
};

// More on interaction testing: https://storybook.js.org/docs/writing-tests/interaction-testing
