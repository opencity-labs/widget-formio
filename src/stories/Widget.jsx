import "./widget.css";
import React from "react";
import ProviderApp from "../Provider";

function Widget({ serviceId, submission, baseUrl, formserverUrl, pdndUrl }) {
  return (
    <section className="storybook-page">
      <ProviderApp
        serviceId={serviceId}
        submission={submission}
        baseUrl={baseUrl}
        formserverUrl={formserverUrl}
        pdndUrl={pdndUrl}
      ></ProviderApp>
    </section>
  );
}

export default Widget;
