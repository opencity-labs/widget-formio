import {
  Accordion,
  AccordionBody,
  AccordionHeader,
  AccordionItem,
  LinkList,
  NavItem,
  NavLink,
} from "design-react-kit";
import { useCallback, useEffect, useState } from "react";
import { useScrollspy } from "../hooks/useScrollspy";
import { getUniqueListBy } from "./util/uniqueList";
import { t } from "i18next";

function InfoSteps({
  percentage,
  flowSteps,
  activeStep,
  formFormio,
  formInstance,
}) {
  // Stato controlla Accordion colonna Info
  const [collapseElementOpen, setCollapseElement] = useState(true);

  // Lista dei info steps
  const [infoSteps, setInfoSteps] = useState([]);

  const [sectionIds, setSectionIds] = useState([]);

  const activeId = useScrollspy(sectionIds, 500);

  // Al click sul info step scrolla all'elemento selezionato
  const onPress = (e) => {
    e.preventDefault();
    const target = window.document.getElementById(
      e.currentTarget.href.split("#")[1],
    );
    if (target) {
      target.scrollIntoView({ behavior: "smooth" });
    }
  };

  const calculateInfoSteps = (page, steps) => {
    if (page?.component?.type === "panel") {
      let idsSection = [];
      page.components.map((element) => {
        if (element.component.type === "fieldset" && element.visible === true) {
          steps.push({
            title: element.component.legend,
            key: `section-${element.component.key}`,
          });
          idsSection.push(`section-${element.component.key}`);
        }
        return steps;
      });

      if (steps.length > 0) {
        setSectionIds(idsSection);
        setInfoSteps(steps);
      } else {
        if (page.component.title) {
          setInfoSteps([
            { title: page.component.title, key: page.component.key },
          ]);
        }
      }
    }
  };
  // Genera gli info step nella colonna a sinistra
  const calculateInfo = useCallback((form) => {
    if (flowSteps[activeStep].type === "summary") {
      const allPagesStep = form?.allPages
        ? form.allPages
        : form?.components
          ? form.components
          : null;
      let steps = [];
      let idsSection = [];
      if (allPagesStep) {
        //Steps pagina Riepilogo
        allPagesStep.map((page) => {
          if (page?.component?.type === "panel") {
            page.components.map((element) => {
              if (
                element.component.type === "fieldset" &&
                element.visible === true
              ) {
                steps.push({
                  title: element.component.legend,
                  key: `section-${element.component.key}`,
                });
                idsSection.push(`section-${element.component.key}`);
              } else {
                if (page.component.title && element.visible === true) {
                  setInfoSteps([
                    { title: page.component.title, key: page.component.key },
                  ]);
                  steps.push({
                    title: page.component.title,
                    key: `panel-${page.component.key}`,
                  });
                }
              }
              return steps;
            });
          }
        });
      }
      if (steps.length > 0) {
        setSectionIds(idsSection);
        setInfoSteps(getUniqueListBy(steps, "key"));
      }
    } else if (flowSteps[activeStep].type === "formio") {
      const allPagesStep = form?.allPages
        ? form.allPages
        : form?.components
          ? form.components
          : null;
      let steps = [];
      if (allPagesStep) {
        calculateInfoSteps(allPagesStep[activeStep], steps);
      }
    }
  });

  useEffect(() => {
    if (
      formFormio &&
      (formInstance?.allPages || formInstance?.components.length)
    ) {
      setTimeout(() => {
        calculateInfo(formInstance);
      }, 300);
    }
  }, [formFormio, activeStep]);

  return (
    <div
      className="cmp-navscroll sticky-top"
      aria-labelledby="accordion-title-one"
    >
      <nav
        className="navbar it-navscroll-wrapper navbar-expand-lg"
        aria-label={t("informationRequired")}
      >
        <div className="navbar-custom" id="navbarNavProgress">
          <div className="menu-wrapper">
            <div className="link-list-wrapper">
              <Accordion>
                <AccordionItem>
                  <AccordionHeader
                    active={collapseElementOpen}
                    onToggle={() => setCollapseElement(!collapseElementOpen)}
                    id={"accordion-title-one"}
                  >
                    {t("informationRequired")}
                  </AccordionHeader>
                  <div className="progress">
                    <div
                      className="progress-bar it-navscroll-progressbar"
                      aria-label="progress bar"
                      role="progressbar"
                      aria-valuenow={percentage.vertical}
                      aria-valuemin="0"
                      aria-valuemax="100"
                      style={{
                        width: percentage.vertical + "%",
                      }}
                    ></div>
                  </div>
                  <AccordionBody active={collapseElementOpen}>
                    <LinkList data-element="page-index">
                      {infoSteps.map((info, idx) => (
                        <NavItem key={`info-${idx}`}>
                          <NavLink
                            href={`#${info.key}`}
                            className={`${info.key}` === activeId && "active"}
                            onClick={(e) => onPress(e)}
                          >
                            <span className="title-medium">
                              {t(info.title)}
                            </span>
                          </NavLink>
                        </NavItem>
                      ))}
                    </LinkList>
                  </AccordionBody>
                </AccordionItem>
              </Accordion>
            </div>
          </div>
        </div>
      </nav>
    </div>
  );
}

export default InfoSteps;
