import { Button, Col, Input, Row } from "design-react-kit";
import { t } from "i18next";
import InputDateTime from "./InputDateTime";
import { useEffect, useState } from "react";
import styled from "styled-components";

const StyledInput = styled(Input)`
  font-size: 1rem;
  font-weight: 700 !important;
  height: 3rem;
  color: #5d7083 !important;
  & .input-group.input-number .input-group-text.align-buttons {
    display: none;
  }
`;

function Stamps({ stamps, setStamps }) {
  return (
    <div className="cmp-card mb-40">
      <div className="card has-bkg-grey shadow-sm">
        <div className="card-header border-0 p-0 m-0">
          <div className="d-flex">
            <h2 className="title-xxlarge mb-1">{t("pagamentoBollo")}</h2>
          </div>
        </div>
        <div className="card-body mt-5 p-0">
          {stamps.map((m, i) => (
            <div key={i} className="p-4 mb-5" style={{ background: "white" }}>
              <Row>
                <Col md="6" xs="12">
                  <div className="form-group bg-white p-3 pb-0  mt-3">
                    <Input
                      type="text"
                      label={t("causale")}
                      id={`"causale"${i}`}
                      value={m.reason}
                      disabled
                    />
                  </div>
                </Col>
                <Col md="6" xs="12">
                  <div className="form-group bg-white p-3 pb-0  mt-3">
                    <StyledInput
                      label={t("importo")}
                      id={`"importo"${i}`}
                      type="currency"
                      addonText="€"
                      value={m.amount.toFixed(2)}
                      disabled
                    />
                  </div>
                </Col>
              </Row>
              <Row>
                <Col md="6" xs="12">
                  <div className="form-group bg-white p-3 pb-0 mt-3">
                    <Input
                      type="text"
                      label={t("numero")}
                      id={`"numero"${i}`}
                      value={stamps[i]?.number}
                      maxLength={14}
                      onChange={(ev) => {
                        let newStamps = [...stamps];
                        newStamps[i].number = ev.target.value;
                        setStamps(newStamps);
                      }}
                    />
                  </div>
                </Col>
                <Col md="6" xs="12">
                  <div className="form-group bg-white p-3 pb-0 mt-3">
                    <InputDateTime
                      type="time"
                      label={t("dataEmissione")}
                      id={`"dataEmissione"${i}`}
                      value={Date.parse(stamps[i]?.emitted_at)}
                      onChange={(val) => {
                        let newStamps = [...stamps];
                        newStamps[i].emitted_at = new Date(
                          val * 1000,
                        ).toISOString();
                        setStamps(newStamps);
                      }}
                    />
                  </div>
                </Col>{" "}
              </Row>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
}

export default Stamps;
