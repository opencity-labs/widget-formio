import React, { useEffect, useRef, useState } from "react";
import Autocomplete from "accessible-autocomplete/react";
import { Icon } from "design-react-kit";

const AccessibleAutocomplete = ({
  value,
  defaultValue = "",
  onConfirm,
  source,
  disabled,
  onClear,
  tClearInput = () => "Clear input",
  ...autocompleteProps
}) => {
  const inputRef = useRef(null);

  const updateInputValue = (val) => {
    setTimeout(() => {
      if (inputRef.current) {
        inputRef.current.elementReferences[-1].value = val;
      }
    }, 0);
  };

  useEffect(() => {
    if (value !== undefined) {
      updateInputValue(value);
    }
  }, [value]);

  useEffect(() => {
    if (disabled) {
      if (inputRef.current) {
        inputRef.current.elementReferences[-1].disabled = true;
      }
    } else {
      if (inputRef.current) {
        inputRef.current.elementReferences[-1].disabled = false;
      }
    }
  }, [disabled]);

  const handleClear = () => {
    onClear && onClear();
  };

  const handleSource = (query, populateResults) => {
    if (
      inputRef.current.elementReferences["-1"].classList.contains(
        "autocomplete__input--focused",
      )
    ) {
      source(query, populateResults);
    }
  };

  const handleOnConfirm = (selectedValue) => {
    if (selectedValue) {
      onConfirm(selectedValue);
    } else {
      updateInputValue(value);
    }
  };

  return (
    <div style={{ position: "relative", width: "100%" }}>
      <Autocomplete
        ref={inputRef}
        {...autocompleteProps}
        defaultValue={defaultValue}
        onConfirm={handleOnConfirm}
        source={handleSource}
      />

      {Boolean(value && value.length > 0 && !disabled) && (
        <button
          onClick={handleClear}
          style={{
            position: "absolute",
            right: "0px",
            top: "50%",
            transform: "translateY(-50%)",
            background: "transparent",
            border: "none",
            cursor: "pointer",
            padding: 0,
          }}
          aria-label={tClearInput()}
        >
          <Icon icon="it-close" size="sm" title={tClearInput()} />
        </button>
      )}
    </div>
  );
};

export default AccessibleAutocomplete;
