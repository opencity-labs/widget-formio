export function epochSecondsToLocalISOString(epochSeconds) {
  return new Date(
    epochSeconds * 1000 +
      -new Date(epochSeconds * 1000).getTimezoneOffset() * 60 * 1000,
  ).toISOString();
}

export function formatISOString(value) {
  return value.replace("Z", "");
}

export function dateTimeToLocalISOString(dateTime) {
  return epochSecondsToLocalISOString(dateTime.getTime() / 1000);
}
