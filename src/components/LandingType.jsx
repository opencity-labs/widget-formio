import F24 from "../pages/LandingPage/F24";
import Payment from "../pages/LandingPage/Payment";
import BuiltIn from "../pages/LandingPage/Built-In";
import General from "../pages/LandingPage/General";
import { RECEIPT_F24 } from "../constants/Receipt";
import Inefficiencies from "../pages/LandingPage/Inefficiencies";

export const LandingType = ({ service, application }) => {
  console.log("service", service);
  if (service?.receipt === RECEIPT_F24) {
    return <F24 application={application} service={service}></F24>;
  } else if (service?.paymentRequired === 1) {
    return <Payment application={application} service={service} />;
  } else if (
    service?.identifier === "inefficiency" ||
    service?.identifier === "inefficiencies"
  ) {
    return (
      <Inefficiencies
        application={application}
        service={service}
      ></Inefficiencies>
    );
  } else if (service?.topics === "built-in") {
    return <BuiltIn application={application} service={service}></BuiltIn>;
  } else {
    return <General application={application} service={service}></General>;
  }
};
