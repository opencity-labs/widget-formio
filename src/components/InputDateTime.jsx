import React, { useState } from "react";
import {
  epochSecondsToLocalISOString,
  formatISOString,
  dateTimeToLocalISOString,
} from "./util/dateTime";
import styled from "styled-components";

function formattedValue(epcohSeconds) {
  return formatISOString(epochSecondsToLocalISOString(epcohSeconds));
}
const StyledInput = styled.input`
  font-size: 1rem;
  font-weight: 700;
  height: 3rem;
`;
export default function InputDateTime({ label, value, id, onChange }) {
  const [epoch, setEpoch] = useState(value);

  const onChangeValue = (e) => {
    console.log("e.target.value: ", e.target.value, e.target.valueAsNumber);
    console.log(
      "val: ",
      new Date(e.target.value).getTime() === e.target.valueAsNumber,
    );
    const val = new Date(e.target.value).getTime() / 1000;
    setEpoch(val);
    onChange(val);
  };

  return (
    <div className="form-group">
      <label for={id} class="active ">
        {label}
      </label>
      <StyledInput
        id={id}
        type="datetime-local"
        class="form-control"
        value={epoch ? formattedValue(epoch) : ""}
        onChange={onChangeValue}
      />
    </div>
  );
}
