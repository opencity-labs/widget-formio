import dayjs from "dayjs";
import { Alert } from "design-react-kit";
import { t } from "i18next";
import { getI18n } from "react-i18next";
var LocalizedFormat = require("dayjs/plugin/localizedFormat");

dayjs.extend(LocalizedFormat);
require("dayjs/locale/de");
require("dayjs/locale/en");
require("dayjs/locale/it");
function ErrorServiceStatus({ serviceStatus }) {
  let msg = t("sospeso");
  if (serviceStatus.status === 4) {
    msg = t("schedulato", {
      dateFrom: dayjs(serviceStatus.scheduledFrom)
        .locale(getI18n().language)
        .format("L LT"),
      dateTo: dayjs(serviceStatus.scheduledTo)
        .locale(getI18n().language)
        .format("L LT"),
    });
  }

  return (
    <div>
      <Alert color="warning">{msg}</Alert>
    </div>
  );
}

export default ErrorServiceStatus;
