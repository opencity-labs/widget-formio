import { Button, Dimmer, DimmerButtons, Fade } from "design-react-kit";
import { baseUrl } from "../_store";

function ErrorAuth() {
  const handleLoginClick = () => {
    window.location.href = `${baseUrl()}/login?return-url=${window.location.href}#/`;
    return null;
  };

  return (
    <Fade className="mt-3 min-height-500" tag="div">
      <Dimmer icon="it-unlocked" className={"align-items-center"}>
        <h4>Non disponi le autorizazzioni necessarie per l'accesso al form</h4>
        <DimmerButtons single>
          <Button color="primary" onClick={handleLoginClick}>
            Accedi
          </Button>
        </DimmerButtons>
      </Dimmer>
    </Fade>
  );
}

export default ErrorAuth;
