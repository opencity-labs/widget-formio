import i18n from "i18next";
import { initReactI18next } from "react-i18next";
import LanguageDetector from "i18next-browser-languagedetector";
import detectLanguageCustom from "../../_helpers/detectLanguage";
import translationEN from "../en.json";
import translationDE from "../de.json";
import translationIT from "../it.json";
const languageDetector = new LanguageDetector();
languageDetector.addDetector(detectLanguageCustom);

// the translations
const resources = {
  it: {
    translation: translationIT,
  },
  en: {
    translation: translationEN,
  },
  de: {
    translation: translationDE,
  },
};

i18n
  .use(languageDetector)
  .use(initReactI18next) // passes i18n down to react-i18next
  .init({
    resources,
    fallbackLng: "it",
    interpolation: {
      escapeValue: false, // react already safes from xss
    },
    detection: {
      // optional htmlTag with lang attribute, the default is:
      htmlTag: document.documentElement,
      // order and from where user language should be detected
      order: ["custom", "cookie", "htmlTag", "localStorage"],
      // keys or params to lookup language from
      lookupCookie: "i18next",
      lookupLocalStorage: "i18next",
      lookupSessionStorage: "i18next",
    },
  });

export default i18n;
