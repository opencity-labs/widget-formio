export class StatusService {
  // Private Fields
  static #ACCESS_LEVEL_ANONYMOUS = 0;
  static #ACCESS_LEVEL_SOCIAL = 1000;
  static #ACCESS_LEVEL_SPID_L1 = 2000;
  static #ACCESS_LEVEL_SPID_L2 = 3000;
  static #ACCESS_LEVEL_CIE = 4000;

  static #STATUS_CANCELLED = 0;
  static #STATUS_AVAILABLE = 1;
  static #STATUS_SUSPENDED = 2;
  static #STATUS_PRIVATE = 3;
  static #STATUS_SCHEDULED = 4;

  // Accessors for "get" functions only (no "set" functions)
  static get ACCESS_LEVEL_ANONYMOUS() {
    return this.#ACCESS_LEVEL_ANONYMOUS;
  }
  static get ACCESS_LEVEL_SOCIAL() {
    return this.#ACCESS_LEVEL_SOCIAL;
  }
  static get ACCESS_LEVEL_SPID_L1() {
    return this.#ACCESS_LEVEL_SPID_L1;
  }
  static get ACCESS_LEVEL_SPID_L2() {
    return this.#ACCESS_LEVEL_SPID_L2;
  }
  static get ACCESS_LEVEL_CIE() {
    return this.#ACCESS_LEVEL_CIE;
  }
  static get STATUS_CANCELLED() {
    return this.#STATUS_CANCELLED;
  }
  static get STATUS_AVAILABLE() {
    return this.#STATUS_AVAILABLE;
  }
  static get STATUS_SUSPENDED() {
    return this.#STATUS_SUSPENDED;
  }
  static get STATUS_PRIVATE() {
    return this.#STATUS_PRIVATE;
  }
  static get STATUS_SCHEDULED() {
    return this.#STATUS_SCHEDULED;
  }
}
