export const convertTimeInInteger = (string) => {
  let concatString = "";
  string.split(":").forEach((time) => {
    concatString += time;
  });
  return parseInt(concatString);
};

export const convertIntegerToTime = (number) => {
  const paddedNumber = number.toString().padStart(4, "0");
  const parts = paddedNumber.match(/.{1,2}/g);
  return parts.join(":");
};
