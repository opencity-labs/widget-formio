import { jwtDecode } from "jwt-decode";
import BasePath from "../_helpers/BasePath";
import Swal from "sweetalert2";
import axios from "axios";
import { sessionGet, sessionSet } from "../_helpers/utilities";
const language = document.documentElement.lang.toString();

class Auth {
  constructor() {
    this.token = null;
    this.basePath = null;
    this.init();
  }

  init() {
    this.token = null;
    this.basePath = new BasePath().getBasePath();
  }

  async getSessionAuthToken() {
    let self = this;
    await axios
      .get(self.basePath + "/api/session-auth")
      .then((res) => {
        //TODO
        debugger;
        self.token = res.data.token;
      })
      .catch((err) => {
        Swal.fire(`'error_message_detail`, `'auth_error'`, "error");
      });
  }

  getSessionAuthTokenPromise() {
    let self = this;
    return new Promise(async (resolve, reject) => {
      if (sessionGet("auth-token")) {
        resolve({ token: sessionGet("auth-token") });
      } else {
        await axios
          .get(self.basePath + "/api/session-auth?with-cookie=true", {
            headers: {
              "Content-Type": "application/json; charset=utf-8",
            },
          })
          .then((res) => {
            const decodedToken = jwtDecode(res.data.token);
            if (decodedToken.exp) {
              sessionSet("auth-token", res.data.token, decodedToken.exp);
            }
          })
          .catch((err) => {
            console.log(err);
          });
      }
    });
  }

  getToken() {
    return this.token;
  }

  execAuthenticatedCall(callback) {
    if (this.token) {
      callback(this.token);
    } else {
      this.getSessionAuthTokenPromise().then((res) => {
        this.token = res.token;
        callback(this.token);
      });
    }
  }
}

export default Auth;
