export const getTitleKey = (slug) => {
  const titleKeyMap = {
    "richiedi-assistenza": "Richiesta inviata",
    segnalazione: "Segnalazione inviata",
  };

  return titleKeyMap[slug];
};

export const getSubTitleKey = (slug) => {
  const titleKeyMap = {
    "richiedi-assistenza":
      "La richiesta di assistenza è stata inviata con successo, sarai ricontattato presto",
    segnalazione: "Grazie, abbiamo ricevuto la tua segnalazione",
    "segnala-disservizio": "Segnalazione n. ",
  };

  return titleKeyMap[slug];
};

export const getApplicationEmail = (application) =>
  application?.data?.email ||
  application?.data?.email_address ||
  (application?.data && application?.data["applicant.data.email_address"]) ||
  null;
