import BasePath from "./../_helpers/BasePath";
import axios from "axios";
import { get } from "lodash-es";

class FormioHelper {
  constructor() {
    this.token = null;
    this.basePath = null;
    this.init();
  }

  init() {
    this.basePath = new BasePath().getBasePath();
  }

  getCurrentLocale() {
    return document.documentElement.lang.toString();
  }

  async getTenantInfo() {
    const response = await axios.get(window.BASE_URL + "/api/tenants/info", {
      headers: {
        "x-locale": this.getCurrentLocale()
      }
    });
    return response.data;
  }

  async authenticatedCall(endPoint) {
    const response = await axios.get(window.BASE_URL + "/api/" + endPoint, {
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + this.token,
      },
    });
    return response.data;
  }

  async getRemoteJson(url, method = "get", headers = null) {
    let config = {};
    if (headers) {
      config = {
        headers: headers,
      };
    }
    const response = await axios({
      method: method,
      url: url,
      params: config,
    });
    return response.data;
  }

  async anonymousCall(endPoint) {
    const response = await axios.get(window.BASE_URL + "/api/" + endPoint, {
      headers: {
        "Content-Type": "application/json",
      },
    });
    return response.data;
  }

  // See Doc {@link https://gitlab.com/opencity-labs/area-personale/core/-/wikis/Guida-alla-creazione-dei-moduli/Form.io-Sdk#esempio-recupero-dato-dai-meta}
  getFieldMeta(getParams) {
    this.getTenantInfo()
      .then((result) => {
        const meta = JSON.parse(result.meta[0]) || null;
        if (meta) {
          if (getParams) {
            return get(meta, getParams, false);
          } else {
            return meta;
          }
        }
      })
      .catch((e) => {
        console.error("error getFieldMeta", e);
      });
  }

  getFieldApplication(getParams) {
    const formioElement = document.querySelector("#formio");
    const applicationId = formioElement.dataset.applicationId;

    return this.authenticatedCall("applications/" + applicationId)
      .then((result) => {
        if (result && getParams) {
          return get(result, getParams);
        } else {
          return result;
        }
      })
      .catch((e) => {
        console.error("error getFieldApplication", e);
      });
  }
}

export default FormioHelper;
