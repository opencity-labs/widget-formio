import r2wc from "@r2wc/react-to-web-component";
import ProviderApp from "./Provider";

const WebAppForm = r2wc(ProviderApp, {
  props: {
    url: "string",
    form: "string",
    serviceId: "string",
    signatureCheckWsUrl: "string",
    hide: "boolean",
    readOnly: "boolean",
    falseProp: "boolean",
    arrayProp: "json",
    submission: "json",
    onChange: "function",
    baseUrl: "string",
    formserverUrl: "string",
    pdndUrl: "string",
  },
});

customElements.define("widget-formio", WebAppForm);
